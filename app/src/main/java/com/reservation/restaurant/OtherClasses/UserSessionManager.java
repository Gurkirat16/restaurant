package com.reservation.restaurant.OtherClasses;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.reservation.restaurant.Activities.LoginActivity;
import com.reservation.restaurant.Beans.AdminBean;
import com.reservation.restaurant.Helper.Utility;

import java.util.HashMap;

/**
 * Created by gurkirat on 20/9/17.
 */

public class UserSessionManager {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context;

    public UserSessionManager(Context context){
        this.context = context;
        sharedPreferences = context.getSharedPreferences(Utility.SHARED_PREF_NAME, Context.MODE_PRIVATE);
        editor = sharedPreferences.edit();
    }

    public void createLoginSession(AdminBean admin){
        editor.putBoolean(Utility.IS_USER_LOGIN, true);
        editor.putString(Utility.SHPEMAIL, admin.getaEmail());
        editor.putString(Utility.SHPPASSWORD, admin.getaPassword());
        editor.putString(Utility.SHPNAME, admin.getaName());
        editor.putString(Utility.SHPRESID, admin.getResId());
        editor.putInt(Utility.SHPID, admin.getaId());
        editor.putString(Utility.SHPTOKEN, admin.getToken());
        editor.commit();
    }
    public void storeToken(String token){
        editor.putString(Utility.SHPTOKEN, token);
    }

    public String getToken(){
        return sharedPreferences.getString(Utility.SHPTOKEN, null );
    }
    public HashMap<String, String> getUserDetails(){
        HashMap user = new HashMap();

        user.put(Utility.SHPEMAIL, sharedPreferences.getString(Utility.SHPEMAIL, null));
        user.put(Utility.SHPPASSWORD, sharedPreferences.getString(Utility.SHPPASSWORD, null));
        user.put(Utility.SHPNAME, sharedPreferences.getString(Utility.SHPNAME, null));
        user.put(Utility.SHPRESID, sharedPreferences.getString(Utility.SHPRESID, null));
        user.put(Utility.SHPTOKEN, sharedPreferences.getString(Utility.SHPTOKEN, null));
        return user;
    }

    public void logoutAdmin(){
        editor.clear();
        editor.commit();

        Intent i = new Intent(context, LoginActivity.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

        context.startActivity(i);
    }

}
