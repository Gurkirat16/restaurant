package com.reservation.restaurant.Adapters;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.R;

import java.util.ArrayList;

/**
 * Created by mitaly on 10/8/17.
 */

public class TablesAdapter extends RecyclerView.Adapter<TablesAdapter.MyViewHolder> {

    ArrayList<Table> listTables;
    int flag;

    public class MyViewHolder extends RecyclerView.ViewHolder{

        TextView txtView;
        public MyViewHolder(View itemView) {
            super(itemView);
            txtView = itemView.findViewById(R.id.txtTableName);
        }
    }

    public TablesAdapter(ArrayList<Table> list, int flag) {
        listTables = list;
        this.flag = flag;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        if(flag == 1)
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tables_resv_details,parent, false);
        else
            view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_simple_table,parent, false);

        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        holder.txtView.setText(listTables.get(position).getTableName());
    }

    @Override
    public int getItemCount() {
        return listTables.size();
    }


}