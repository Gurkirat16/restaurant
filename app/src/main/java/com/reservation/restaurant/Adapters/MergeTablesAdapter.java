package com.reservation.restaurant.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservation.restaurant.Beans.MergedTable;
import com.reservation.restaurant.R;

import java.util.ArrayList;

/**
 * Created by mitaly on 9/10/17.
 */

public class MergeTablesAdapter extends RecyclerView.Adapter<MergeTablesAdapter.MyViewHolder> {

    ArrayList<MergedTable> listTables;
    int rowIndex = -1;

    public MergeTablesAdapter() {
    }

    public void setData(ArrayList<MergedTable> list){
        if(listTables != list){
            listTables = list;
            notifyDataSetChanged();
        }
    }
    public void setRowIndex(int index){
        this.rowIndex = index;
    }
    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtMergeTableName;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtMergeTableName = (TextView) itemView.findViewById(R.id.txtMergeTableName);
        }
    }

    @Override
    public MergeTablesAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_merge_tables, parent, false);;
        return new MergeTablesAdapter.MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MergeTablesAdapter.MyViewHolder holder, int position) {
        holder.txtMergeTableName.setText(listTables.get(position).getTableName());

    }

    @Override
    public int getItemCount() {
        return listTables.size();
    }


}
