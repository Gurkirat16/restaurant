package com.reservation.restaurant.Adapters;

import android.app.Dialog;
import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.reservation.restaurant.Activities.CreateCategoryActivity;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.R;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by gurkirat on 14/8/17.
 */

public class AdminCategoriesAdapter extends RecyclerSwipeAdapter<AdminCategoriesAdapter.SimpleViewHolder> {

    List<TableCategoryBean> adminCategories;
    Context mContext;
    Dialog pd;
    ArrayList<String> numTablesList;

    public AdminCategoriesAdapter(Context context, List<TableCategoryBean> adminCategories, ArrayList<String> numTablesList){
        this.mContext = context;
        this.adminCategories = adminCategories;
        this.numTablesList = numTablesList;
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_admin_categories, parent, false);
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final SimpleViewHolder viewHolder, final int position) {
        final TableCategoryBean item = adminCategories.get(position);

        viewHolder.tvCatNames.setText(item.getcName());
        //String strNumTable;
        try{
            if (numTablesList.get(position) == null || numTablesList.get(position).equals("0"))
                viewHolder.tvNumTables.setText("No Tables");
            else if (numTablesList.get(position).equals("1"))
                viewHolder.tvNumTables.setText("1 Table");
            else
                viewHolder.tvNumTables.setText(numTablesList.get(position) + " Tables");
        }catch (IndexOutOfBoundsException e){
            viewHolder.tvNumTables.setText("No Tables");
        }


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        //viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));
        viewHolder.swipeLayout.setRightSwipeEnabled(true);
        viewHolder.swipeLayout.setLeftSwipeEnabled(false);
        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));


        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        /*viewHolder.swipeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if ((((SwipeLayout) v).getOpenStatus() == SwipeLayout.Status.Close)) {
                    //Start your activity

                    Toast.makeText(mContext, " onClick : " + item.getName() + " \n" + item.getEmailId(), Toast.LENGTH_SHORT).show();
                }

            }
        });*/


        viewHolder.btnEditCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pd = Utility.initDialog(mContext);
                ((CreateCategoryActivity)mContext).editCategory(position);
            }
        });


        /*viewHolder.btnDeleteCat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateCategoryActivity)mContext).deleteCategory(position, viewHolder.swipeLayout);
            }
        });*/

        viewHolder.btnViewTables.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateCategoryActivity)mContext).viewTables(position);
            }
        });

        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return adminCategories.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }


    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        AppCompatImageView btnEditCat;
//        AppCompatImageView btnDeleteCat;
        AppCompatImageView btnViewTables;
        TextView tvCatNames;
        TextView tvNumTables;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipe);
            tvCatNames = (TextView) itemView.findViewById(R.id.categoryName);
            tvNumTables = (TextView) itemView.findViewById(R.id.numTables);
            btnEditCat = (AppCompatImageView) itemView.findViewById(R.id.btnEditCat);
//            btnDeleteCat = (AppCompatImageView) itemView.findViewById(R.id.btnDeleteCat);
            btnViewTables = (AppCompatImageView) itemView.findViewById(R.id.btnViewTables);

        }
    }
}