package com.reservation.restaurant.Adapters;

import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.reservation.restaurant.Beans.DateItem;
import com.reservation.restaurant.Beans.GeneralItem;
import com.reservation.restaurant.Beans.ListItem;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.R;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mitaly on 16/8/17.
 */

public class ReservationsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<ListItem> consolidatedList = new ArrayList<>();
    //0- recent resv ; 1- group acc to date
    private int identifer;

    public ReservationsAdapter(ArrayList<ListItem> consolidatedList, int identifer) {
        Log.i("show", "list in adapter: "+consolidatedList.toString());
        this.consolidatedList = consolidatedList;
        this.identifer = identifer;
    }

    @Override
    public int getItemCount() {
        return consolidatedList != null? consolidatedList.size() : 0;
    }

    @Override
    public int getItemViewType(int position) {
        return consolidatedList.get(position).getType();
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType){
            case ListItem.TYPE_GENERAL:
                View v1 = null;
                if(identifer == 1){
                    v1 = inflater.inflate(R.layout.list_item_resv_info_date, parent, false);
                    viewHolder = new GeneralItemViewHolder(v1);
                }
                else if(identifer == 0) {
                    v1 = inflater.inflate(R.layout.list_item_resv_recent, parent, false);
                    viewHolder = new RecentItemViewHolder(v1);
                }
                break;
            case ListItem.TYPE_DATE:
                View v2 = inflater.inflate(R.layout.list_item_resv_date, parent, false);
                viewHolder = new DateItemViewHolder(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        switch (holder.getItemViewType()) {

            case ListItem.TYPE_GENERAL:
                GeneralItem generalItem = (GeneralItem) consolidatedList.get(position);
                String strTime = generalItem.getRev().getrStartTime();
                String strDate = generalItem.getRev().getrDate();
                SimpleDateFormat stfo = new SimpleDateFormat("HH:mm:ss");
                SimpleDateFormat stfn = new SimpleDateFormat("HH:mm");
                SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdfn = new SimpleDateFormat("d MMM, yyyy");
                Date date;
                Date time;
                try{
                    time = stfo.parse(strTime);
                    strTime = stfn.format(time);
                    date = sdfo.parse(strDate);
                    strDate = sdfn.format(date);
                }catch(ParseException e){
                    e.printStackTrace();
                }
                if(identifer == 1){
                    GeneralItemViewHolder generalViewHolder = (GeneralItemViewHolder) holder;
                    generalViewHolder.txtViewTime.setText(strTime);
                    generalViewHolder.txtViewCusName.setText(generalItem.getRev().getrCusInfo().getCusName());
                    generalViewHolder.txtViewMembers.setText(String.valueOf(generalItem.getRev().getrCusInfo().getNumMembers()));

                }else if(identifer == 0){
                    RecentItemViewHolder recentItemViewHolder = (RecentItemViewHolder)holder;
                    recentItemViewHolder.txtViewDate.setText(strDate);
                    recentItemViewHolder.txtViewTime.setText(strTime);
                    recentItemViewHolder.txtViewCusName.setText(generalItem.getRev().getrCusInfo().getCusName());
                    recentItemViewHolder.txtViewMembers.setText(String.valueOf(generalItem.getRev().getrCusInfo().getNumMembers()));
                }

                break;

            case ListItem.TYPE_DATE:
                DateItem dateItem = (DateItem) consolidatedList.get(position);
                String strDate1 = dateItem.getDate();
                SimpleDateFormat sdfo1 = new SimpleDateFormat("yyyy-MM-dd");
                SimpleDateFormat sdfn1 = new SimpleDateFormat("d MMM, yyyy");
                Date date1;
                try{
                    date1 = sdfo1.parse(strDate1);
                    strDate1 = sdfn1.format(date1);
                }catch(ParseException e){
                    e.printStackTrace();
                }

                DateItemViewHolder dateViewHolder = (DateItemViewHolder) holder;
                dateViewHolder.txtViewDate.setText(strDate1);
                break;
        }
    }

    public class RecentItemViewHolder extends RecyclerView.ViewHolder{

        TextView txtViewDate;
        TextView txtViewTime;
        TextView txtViewCusName;
        TextView txtViewMembers;

        public RecentItemViewHolder(View itemView) {
            super(itemView);
            txtViewCusName = (TextView)itemView.findViewById(R.id.resvCusName);
            txtViewTime = (TextView)itemView.findViewById(R.id.resvTime);
            txtViewDate = (TextView)itemView.findViewById(R.id.resvDate);
            txtViewMembers = (TextView)itemView.findViewById(R.id.resvMembers);
        }
    }

    public class DateItemViewHolder extends RecyclerView.ViewHolder{

        TextView txtViewDate;
        public DateItemViewHolder(View itemView) {
            super(itemView);
            txtViewDate = (TextView)itemView.findViewById(R.id.resvDate);
        }
    }

    public class GeneralItemViewHolder extends RecyclerView.ViewHolder{
        TextView txtViewTime;
        TextView txtViewCusName;
        TextView txtViewMembers;

        public GeneralItemViewHolder(View itemView) {
            super(itemView);
            txtViewCusName = (TextView)itemView.findViewById(R.id.resvCusName);
            txtViewTime = (TextView)itemView.findViewById(R.id.resvTime);
            txtViewMembers = (TextView)itemView.findViewById(R.id.resvMembers);
        }
    }

}
