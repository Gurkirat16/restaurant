package com.reservation.restaurant.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.reservation.restaurant.Activities.ListTablesActivity;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.R;

import java.util.ArrayList;

/**
 * Created by gurkirat on 9/11/17.
 */

public class ListTablesAdapter extends RecyclerSwipeAdapter<ListTablesAdapter.SimpleViewHolder> {

    ArrayList<Table> listTables;
    Context mContext;

    public ListTablesAdapter(Context context, ArrayList<Table> list) {
        this.mContext = context;
        listTables = list;
    }

    @Override
    public ListTablesAdapter.SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_tables, parent, false);
        return new ListTablesAdapter.SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ListTablesAdapter.SimpleViewHolder viewHolder, final int position) {
        final Table item = listTables.get(position);

        viewHolder.tvTableNames.setText(item.getTableName());
        //viewHolder.tvNumTables.setText(item.getEmailId());


        viewHolder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        // Drag From Left
        //viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper));
        viewHolder.swipeLayout.setRightSwipeEnabled(true);
        viewHolder.swipeLayout.setLeftSwipeEnabled(false);
        // Drag From Right
        viewHolder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, viewHolder.swipeLayout.findViewById(R.id.bottom_wrapper_table));


        // Handling different events when swiping
        viewHolder.swipeLayout.addSwipeListener(new SwipeLayout.SwipeListener() {
            @Override
            public void onClose(SwipeLayout layout) {
                //when the SurfaceView totally cover the BottomView.
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                //you are swiping.
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {

            }

            @Override
            public void onOpen(SwipeLayout layout) {
                //when the BottomView totally show.
            }

            @Override
            public void onStartClose(SwipeLayout layout) {

            }

            @Override
            public void onHandRelease(SwipeLayout layout, float xvel, float yvel) {
                //when user's hand released.
            }
        });

        viewHolder.btnEditTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListTablesActivity)mContext).editTable(position);
            }
        });

/*
        viewHolder.btnDeleteTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((ListTablesActivity)mContext).deleteTable(position, viewHolder.swipeLayout);
            }
        });*/

        // mItemManger is member in RecyclerSwipeAdapter Class
        mItemManger.bindView(viewHolder.itemView, position);

    }

    @Override
    public int getItemCount() {
        return listTables.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipeTables;
    }

    //  ViewHolder Class

    public static class SimpleViewHolder extends RecyclerView.ViewHolder {
        SwipeLayout swipeLayout;
        AppCompatImageView btnEditTable;
//        AppCompatImageView btnDeleteTable;
        TextView tvTableNames;

        public SimpleViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout) itemView.findViewById(R.id.swipeTables);
            tvTableNames = (TextView) itemView.findViewById(R.id.txtTableName);
            btnEditTable = (AppCompatImageView) itemView.findViewById(R.id.btnEditTable);
//            btnDeleteTable = (AppCompatImageView) itemView.findViewById(R.id.btnDeleteTable);
        }
    }


}
