package com.reservation.restaurant.Adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.TextView;

import com.reservation.restaurant.Activities.MergeCmbActivity;
import com.reservation.restaurant.Beans.MergeCmb;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.R;
import java.util.ArrayList;


/**
 * Created by mitaly on 8/10/17.
 */

public class MergeCombnAdapter extends RecyclerView.Adapter<MergeCombnAdapter.MyViewHolder> {

    ArrayList<MergeCmb> listCmb;
    Context context;
    MergeTablesAdapter mergeTablesAdapter, adapter;

    public MergeCombnAdapter(ArrayList<MergeCmb> listCmb, Context context) {
        this.listCmb = listCmb;
        this.context = context;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        TextView txtMergeTables;
        RecyclerView rcvMergeTables;
        AppCompatImageView deleteCmb;

        public MyViewHolder(View itemView) {
            super(itemView);
            txtMergeTables = (TextView)itemView.findViewById(R.id.txtMergeTables);
            rcvMergeTables = (RecyclerView)itemView.findViewById(R.id.rcvMergeTables);
            deleteCmb = (AppCompatImageView) itemView.findViewById(R.id.deleteCmb);

            mergeTablesAdapter = new MergeTablesAdapter();
            rcvMergeTables.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false));
            rcvMergeTables.setItemAnimator(new DefaultItemAnimator());
            rcvMergeTables.setAdapter(mergeTablesAdapter);

            deleteCmb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Log.i("show", "adapter pos: "+getAdapterPosition());
                    ((MergeCmbActivity)context).deleteMergeCmb(getAdapterPosition());
                }
            });
            rcvMergeTables.addOnItemTouchListener(new RecyclerItemClickListener(context,
                    rcvMergeTables, new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                   // adapter = mergeTablesAdapter;
                    //((MergeCmbActivity)context).deleteMergedTable(position, getAdapterPosition());
                    Log.i("show", "on itemClick: "+position+" : "+getAdapterPosition());
                }

                @Override
                public void onLongItemClick(View view, int position) {

                }
            }));
        }
    }
    public void notifyChildAdapter(int childPosClicked){
        adapter.notifyItemRemoved(childPosClicked);
    }
    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_merge_cmbn, parent, false);;
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        holder.txtMergeTables.setText("Combination "+(position+1));
        MergeCmb mergeCmb = listCmb.get(position);
        mergeTablesAdapter.setData(mergeCmb.getList());
        mergeTablesAdapter.setRowIndex(position);
    }

    @Override
    public int getItemCount() {
        return listCmb.size();
    }

}
