package com.reservation.restaurant.Adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gurkirat on 11/8/17.
 */

public class GlobalCategoriesAdapter extends RecyclerView.Adapter<GlobalCategoriesAdapter.ViewHolder>{

    List<String> globalCategories;
    ArrayList<String> categoriesList;

    public GlobalCategoriesAdapter(List<String> globalCategories){
        this.globalCategories = globalCategories;
        categoriesList = new ArrayList<>();
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        CheckBox cbCategory;

        public ViewHolder(View itemView){
            super(itemView);
            cbCategory = (CheckBox)itemView.findViewById(R.id.cbGlobalCategory);
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_global_categories, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        holder.cbCategory.setText(globalCategories.get(position));
        holder.cbCategory.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    categoriesList.add(holder.cbCategory.getText().toString());
                }else{
                    categoriesList.remove(holder.cbCategory.getText().toString());
                }
            }
        });
    }

    public ArrayList<String> getCategoriesList(){
        return categoriesList;
    }

    @Override
    public int getItemCount() {
        return globalCategories.size();
    }

}
