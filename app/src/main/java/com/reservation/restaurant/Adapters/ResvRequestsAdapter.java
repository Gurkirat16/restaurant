package com.reservation.restaurant.Adapters;

import android.content.Context;
import android.media.Image;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;

import com.daimajia.swipe.adapters.RecyclerSwipeAdapter;
import com.reservation.restaurant.Activities.MergeCmbActivity;
import com.reservation.restaurant.Activities.RsvRequestsActivity;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Fragments.FragmentResvRequests;
import com.reservation.restaurant.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by mitaly on 28/10/17.
 */

public class ResvRequestsAdapter extends RecyclerSwipeAdapter<ResvRequestsAdapter.MyViewHolder>  {
    ArrayList<Reservations> list;
    Context context;
    Fragment fragment;

    public ResvRequestsAdapter(ArrayList<Reservations> list, Context context, Fragment fragment) {
        this.list = list;
        this.context = context;
        this.fragment = fragment;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_resv_request, parent, false);
        return new MyViewHolder(view);
    }
    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        final Reservations resv = list.get(position);

        String strStartTime = resv.getrStartTime();
        String strDate = resv.getrDate();
        SimpleDateFormat stfo = new SimpleDateFormat("HH:mm:ss");
        SimpleDateFormat stfn = new SimpleDateFormat("HH:mm");
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfn = new SimpleDateFormat("d MMM, yyyy");
        Date date;
        Date startTime;
        try{
            startTime = stfo.parse(strStartTime);
            strStartTime = stfn.format(startTime);
            date = sdfo.parse(strDate);
            strDate = sdfn.format(date);
        }catch(ParseException e){
            e.printStackTrace();
        }
        holder.txtViewTime.setText(strStartTime);
        holder.txtViewDate.setText(strDate);
        holder.txtViewCusName.setText(resv.getrCusInfo().getCusName());
        holder.txtViewCategory.setText(resv.getrCategoryInfo().getcName());

        holder.swipeLayout.setShowMode(SwipeLayout.ShowMode.PullOut);

        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Right, holder.itemView.findViewById(R.id.bottomWrapperAccept));
        holder.swipeLayout.addDrag(SwipeLayout.DragEdge.Left, holder.itemView.findViewById(R.id.bottomWrapperReject));

        holder.swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
             //   YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.trash));
            }
        });
        holder.swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
            }
        });
        holder.swipeLayout.getSurfaceView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                mItemManger.closeAllItems();
                ((FragmentResvRequests)fragment).showDetailsActivity(position);
            }
        });
        holder.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                mItemManger.closeAllItems();
                ((FragmentResvRequests)fragment).actionResvReq(1,list.get(position),position);
            }
        });
        holder.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mItemManger.removeShownLayouts(holder.swipeLayout);
                mItemManger.closeAllItems();
                ((FragmentResvRequests)fragment).actionResvReq(2,list.get(position), position);
            }
        });

        mItemManger.bindView(holder.itemView, position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder{
        SwipeLayout swipeLayout;
        TextView txtViewDate;
        TextView txtViewTime;
        TextView txtViewCategory;
        TextView txtViewCusName;
        AppCompatImageView btnAccept, btnReject;

        public MyViewHolder(View itemView) {
            super(itemView);
            swipeLayout = (SwipeLayout)itemView.findViewById(R.id.swipe);
            //Bottom views
            btnAccept = (AppCompatImageView) itemView.findViewById(R.id.btnAccept);
            btnReject = (AppCompatImageView) itemView.findViewById(R.id.btnReject);
            //Foreground views
            txtViewCategory = (TextView)itemView.findViewById(R.id.resvCategory);
            txtViewTime = (TextView)itemView.findViewById(R.id.revTime);
            txtViewDate = (TextView)itemView.findViewById(R.id.resvDate);
            txtViewCusName = (TextView)itemView.findViewById(R.id.resvCusName);
        }
    }



}
