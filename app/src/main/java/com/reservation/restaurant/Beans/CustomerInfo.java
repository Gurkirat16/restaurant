package com.reservation.restaurant.Beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mitaly on 27/10/17.
 */

public class CustomerInfo implements Parcelable{
    int cusId;
    String cusName;
    String cusEmail;
    String cusPhone;
    int numMembers;
    int rCmbId;

    public CustomerInfo() {
    }

    public CustomerInfo(int cusId, String cusName, String cusEmail, String cusPhone, int numMembers, int rCmbId) {
        this.cusId = cusId;
        this.cusName = cusName;
        this.cusEmail = cusEmail;
        this.cusPhone = cusPhone;
        this.numMembers = numMembers;
        this.rCmbId = rCmbId;
    }

    protected CustomerInfo(Parcel in) {
        cusId = in.readInt();
        cusName = in.readString();
        cusEmail = in.readString();
        cusPhone = in.readString();
        numMembers = in.readInt();
        rCmbId = in.readInt();
    }


    public static final Creator<CustomerInfo> CREATOR = new Creator<CustomerInfo>() {
        @Override
        public CustomerInfo createFromParcel(Parcel in) {
            return new CustomerInfo(in);
        }

        @Override
        public CustomerInfo[] newArray(int size) {
            return new CustomerInfo[size];
        }
    };

    public int getCusId() {
        return cusId;
    }

    public void setCusId(int cusId) {
        this.cusId = cusId;
    }

    public String getCusName() {
        return cusName;
    }

    public void setCusName(String cusName) {
        this.cusName = cusName;
    }

    public String getCusEmail() {
        return cusEmail;
    }

    public void setCusEmail(String cusEmail) {
        this.cusEmail = cusEmail;
    }

    public String getCusPhone() {
        return cusPhone;
    }

    public void setCusPhone(String cusPhone) {
        this.cusPhone = cusPhone;
    }

    public int getNumMembers() {
        return numMembers;
    }

    public void setNumMembers(int numMembers) {
        this.numMembers = numMembers;
    }

    public int getrCmbId() {
        return rCmbId;
    }

    public void setrCmbId(int rCmbId) {
        this.rCmbId = rCmbId;
    }

    @Override
    public String toString() {
        return "CustomerInfo{" +
                "cusId=" + cusId +
                ", cusName='" + cusName + '\'' +
                ", cusEmail='" + cusEmail + '\'' +
                ", cusPhone='" + cusPhone + '\'' +
                ", numMembers=" + numMembers +
                ", rCmbId=" + rCmbId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(cusId);
        parcel.writeString(cusName);
        parcel.writeString(cusEmail);
        parcel.writeString(cusPhone);
        parcel.writeInt(numMembers);
        parcel.writeInt(rCmbId);
    }
}
