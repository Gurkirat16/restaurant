package com.reservation.restaurant.Beans;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mitaly on 11/10/17.
 */

public class MergeCmb{
    int cmbId;
    ArrayList<MergedTable> list;

    public MergeCmb(List<Table> items, int cmbId) {
        this.list = (ArrayList)items;
        this.cmbId = cmbId;
    }

    public int getCmbId() {
        return cmbId;
    }

    public void setCmbId(int cmbId) {
        this.cmbId = cmbId;
    }

    public ArrayList<MergedTable> getList() {
        return list;
    }

    public void setList(ArrayList<MergedTable> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "MergeCmb{" +
                "cmbId=" + cmbId +
                ", list=" + list +
                '}';
    }
}
