package com.reservation.restaurant.Beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by gurkirat on 10/8/17.
 */

public class TableCategoryBean implements Parcelable{

    int cId;
    String cName;
    int resId;
    String cImg;
    String cDesc;

    public TableCategoryBean(){

    }

    public TableCategoryBean(String cImg){
        this.cImg = cImg;
    }

    public TableCategoryBean(int cId, String cName, int resId, String cImg, String cDesc) {
        this.cId = cId;
        this.cName = cName;
        this.resId = resId;
        this.cImg = cImg;
        this.cDesc = cDesc;
    }

    protected TableCategoryBean(Parcel in) {
        cId = in.readInt();
        cName = in.readString();
        resId = in.readInt();
        cImg = in.readString();
        cDesc = in.readString();
    }

    public static final Creator<TableCategoryBean> CREATOR = new Creator<TableCategoryBean>() {
        @Override
        public TableCategoryBean createFromParcel(Parcel in) {
            return new TableCategoryBean(in);
        }

        @Override
        public TableCategoryBean[] newArray(int size) {
            return new TableCategoryBean[size];
        }
    };

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public String getcName() {
        return cName;
    }

    public void setcName(String cName) {
        this.cName = cName;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getcImg() {
        return cImg;
    }

    public void setcImg(String cImg) {
        this.cImg = cImg;
    }

    public String getcDesc() {
        return cDesc;
    }

    public void setcDesc(String cDesc) {
        this.cDesc = cDesc;
    }

    @Override
    public String toString() {
        return "TableCategoryBean{" +
                "cId=" + cId +
                ", cName='" + cName + '\'' +
                ", resId=" + resId +
                ", cImg='" + cImg + '\'' +
                ", cDesc='" + cDesc + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(cId);
        parcel.writeString(cName);
        parcel.writeInt(resId);
        parcel.writeString(cImg);
        parcel.writeString(cDesc);
    }
}