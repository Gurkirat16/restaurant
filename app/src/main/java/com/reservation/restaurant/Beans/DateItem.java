package com.reservation.restaurant.Beans;

import android.annotation.SuppressLint;
import android.os.Parcel;

/**
 * Created by mitaly on 16/8/17.
 */

@SuppressLint("ParcelCreator")
public class DateItem extends ListItem{
    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public int getType() {
        return TYPE_DATE;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
