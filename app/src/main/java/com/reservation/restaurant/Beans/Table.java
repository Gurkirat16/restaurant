package com.reservation.restaurant.Beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mitaly on 10/8/17.
 */

public class Table implements Parcelable{
    int tId;
    int cId;
    String tableName;
    int numChairs;
    int statusMergeable;
    int statusActive;

    public Table(){
         tId = 0;
         cId = 0;
         tableName = "";
         numChairs = 0;
         statusMergeable = 0;
         statusActive = 1;
    }

    public Table(int tId, int cId, String tableName, int numChairs, int statusMergeable, int statusActive) {
        this.tId = tId;
        this.cId = cId;
        this.tableName = tableName;
        this.numChairs = numChairs;
        this.statusMergeable = statusMergeable;
        this.statusActive = statusActive;
    }

    protected Table(Parcel in) {
        tId = in.readInt();
        cId = in.readInt();
        tableName = in.readString();
        numChairs = in.readInt();
        statusMergeable = in.readInt();
        statusActive = in.readInt();
    }

    public static final Creator<Table> CREATOR = new Creator<Table>() {
        @Override
        public Table createFromParcel(Parcel in) {
            return new Table(in);
        }

        @Override
        public Table[] newArray(int size) {
            return new Table[size];
        }
    };

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getNumChairs() {
        return numChairs;
    }

    public void setNumChairs(int numChairs) {
        this.numChairs = numChairs;
    }

    public int getStatusMergeable() {
        return statusMergeable;
    }

    public void setStatusMergeable(int statusMergeable) {
        this.statusMergeable = statusMergeable;
    }

    public int getStatusActive() {
        return statusActive;
    }

    public void setStatusActive(int statusActive) {
        this.statusActive = statusActive;
    }

    @Override
    public String toString() {
        return "Table{" +
                "tId=" + tId +
                ", cId=" + cId +
                ", tableName='" + tableName + '\'' +
                ", numChairs=" + numChairs +
                ", statusMergeable=" + statusMergeable +
                ", statusActive=" + statusActive +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(tId);
        parcel.writeInt(cId);
        parcel.writeString(tableName);
        parcel.writeInt(numChairs);
        parcel.writeInt(statusMergeable);
        parcel.writeInt(statusActive);
    }
}

