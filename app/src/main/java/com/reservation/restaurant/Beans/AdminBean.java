package com.reservation.restaurant.Beans;

/**
 * Created by gurkirat on 22/9/17.
 */

public class AdminBean {
    int aId;
    String aName, aEmail, aPassword,resId, aToken;

    public AdminBean(int aId, String aName, String aEmail, String aPassword, String resId, String aToken) {
        this.aId = aId;
        this.aName = aName;
        this.aEmail = aEmail;
        this.aPassword = aPassword;
        this.resId = resId;
        this.aToken = aToken;
    }

    public int getaId() {
        return aId;
    }

    public void setaId(int aId) {
        this.aId = aId;
    }

    public String getaName() {
        return aName;
    }

    public void setaName(String aName) {
        this.aName = aName;
    }

    public String getaEmail() {
        return aEmail;
    }

    public void setaEmail(String aEmail) {
        this.aEmail = aEmail;
    }

    public String getaPassword() {
        return aPassword;
    }

    public void setaPassword(String aPassword) {
        this.aPassword = aPassword;
    }

    public String getResId() {
        return resId;
    }

    public void setResId(String resId) {
        this.resId = resId;
    }

    public String getToken() {
        return aToken;
    }

    public void setToken(String aToken) {
        this.aToken = aToken;
    }

    @Override
    public String toString() {
        return "AdminBean{" +
                "aId=" + aId +
                ", aName='" + aName + '\'' +
                ", aEmail='" + aEmail + '\'' +
                ", aPassword='" + aPassword + '\'' +
                ", resId='" + resId + '\'' +
                ", token='" + aToken + '\'' +
                '}';
    }
}
