package com.reservation.restaurant.Beans;

/**
 * Created by mitaly on 1/10/17.
 */

public class TableFloormap {
    int tId;
    String tableName;
    int icon;

    public TableFloormap() {
    }

    public TableFloormap(int tId, String tableName, int icon) {
        this.tId = tId;
        this.tableName = tableName;
        this.icon = icon;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public int getIcon() {
        return icon;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    @Override
    public String toString() {
        return "TableFloormap{" +
                "tId=" + tId +
                ", tableName='" + tableName + '\'' +
                ", icon=" + icon +
                '}';
    }
}
