package com.reservation.restaurant.Beans;

import android.os.Parcel;
import android.os.Parcelable;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

/**
 * Created by mitaly on 16/8/17.
 */

public class Reservations implements Parcelable{
    String rDate;
    String rStartTime;
    String rEndTime;
    int rStatus;
    int rCmbId;
    int resId;
    CustomerInfo rCusInfo;
    TableCategoryBean rCategoryInfo;
    ArrayList<Table> arrTableInfo;

    public Reservations() {
        rDate = null;
        rStartTime = null;
        rEndTime = null;
        rStatus = -1;
        rCmbId = -1;
        resId = -1;
        rCategoryInfo = null;
        arrTableInfo = null;
        rCusInfo = null;
    }

    public Reservations(String rDate, String rStartTime, String rEndTime, int rStatus, int rCmbId, int resId, CustomerInfo rCusInfo, TableCategoryBean rCategoryInfo, ArrayList<Table> arrTableInfo) {
        this.rDate = rDate;
        this.rStartTime = rStartTime;
        this.rEndTime = rEndTime;
        this.rStatus = rStatus;
        this.rCmbId = rCmbId;
        this.resId = resId;
        this.rCusInfo = rCusInfo;
        this.rCategoryInfo = rCategoryInfo;
        this.arrTableInfo = arrTableInfo;
    }

    protected Reservations(Parcel in) {
        rDate = in.readString();
        rStartTime = in.readString();
        rEndTime = in.readString();
        rStatus = in.readInt();
        rCmbId = in.readInt();
        resId = in.readInt();
        rCusInfo = in.readParcelable(CustomerInfo.class.getClassLoader());
        rCategoryInfo = in.readParcelable(TableCategoryBean.class.getClassLoader());
        arrTableInfo = in.createTypedArrayList(Table.CREATOR);
    }

    public static final Creator<Reservations> CREATOR = new Creator<Reservations>() {
        @Override
        public Reservations createFromParcel(Parcel in) {
            return new Reservations(in);
        }

        @Override
        public Reservations[] newArray(int size) {
            return new Reservations[size];
        }
    };

    public String getrDate() {
        return rDate;
    }

    public void setrDate(String rDate) {
        this.rDate = rDate;
    }

    public String getrStartTime() {
        return rStartTime;
    }

    public void setrStartTime(String rStartTime) {
        this.rStartTime = rStartTime;
    }

    public String getrEndTime() {
        return rEndTime;
    }

    public void setrEndTime(String rEndTime) {
        this.rEndTime = rEndTime;
    }

    public int getrStatus() {
        return rStatus;
    }

    public void setrStatus(int rStatus) {
        this.rStatus = rStatus;
    }

    public int getrCmbId() {
        return rCmbId;
    }

    public void setrCmbId(int rCmbId) {
        this.rCmbId = rCmbId;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public CustomerInfo getrCusInfo() {
        return rCusInfo;
    }

    public void setrCusInfo(CustomerInfo rCusInfo) {
        this.rCusInfo = rCusInfo;
    }

    public TableCategoryBean getrCategoryInfo() {
        return rCategoryInfo;
    }

    public void setrCategoryInfo(TableCategoryBean rCategoryInfo) {
        this.rCategoryInfo = rCategoryInfo;
    }

    public ArrayList<Table> getArrTableInfo() {
        return arrTableInfo;
    }

    public void setArrTableInfo(ArrayList<Table> arrTableInfo) {
        this.arrTableInfo = arrTableInfo;
    }

    @Override
    public String toString() {
        return "Reservations{" +
                "rDate='" + rDate + '\'' +
                ", rStartTime='" + rStartTime + '\'' +
                ", rEndTime='" + rEndTime + '\'' +
                ", rStatus=" + rStatus +
                ", rCmbId=" + rCmbId +
                ", resId=" + resId +
                ", rCusInfo=" + rCusInfo +
                ", rCategoryInfo=" + rCategoryInfo +
                ", arrTableInfo=" + arrTableInfo +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(rDate);
        dest.writeString(rStartTime);
        dest.writeString(rEndTime);
        dest.writeInt(rStatus);
        dest.writeInt(rCmbId);
        dest.writeInt(resId);
        dest.writeParcelable(rCusInfo, flags);
        dest.writeParcelable(rCategoryInfo, flags);
        dest.writeTypedList(arrTableInfo);
    }




}
