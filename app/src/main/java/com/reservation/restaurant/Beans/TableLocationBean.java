package com.reservation.restaurant.Beans;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by gurkirat on 29/9/17.
 */

public class TableLocationBean implements Parcelable{

    int locId;
    int tId;
    int cId;
    float x_coord;
    float y_coord;
    int resId;

    public TableLocationBean(){

    }

    public TableLocationBean(int locId, int tId, int cId, float x_coord, float y_coord, int resId) {
        this.locId = locId;
        this.tId = tId;
        this.cId = cId;
        this.x_coord = x_coord;
        this.y_coord = y_coord;
        this.resId = resId;
    }

    protected TableLocationBean(Parcel in) {
        locId = in.readInt();
        tId = in.readInt();
        cId = in.readInt();
        x_coord = in.readFloat();
        y_coord = in.readFloat();
        resId = in.readInt();
    }

    public static final Creator<TableLocationBean> CREATOR = new Creator<TableLocationBean>() {
        @Override
        public TableLocationBean createFromParcel(Parcel in) {
            return new TableLocationBean(in);
        }

        @Override
        public TableLocationBean[] newArray(int size) {
            return new TableLocationBean[size];
        }
    };

    public int getLocId() {
        return locId;
    }

    public void setLocId(int locId) {
        this.locId = locId;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public int getcId() {
        return cId;
    }

    public void setcId(int cId) {
        this.cId = cId;
    }

    public float getX_coord() {
        return x_coord;
    }

    public void setX_coord(float x_coord) {
        this.x_coord = x_coord;
    }

    public float getY_coord() {
        return y_coord;
    }

    public void setY_coord(float y_coord) {
        this.y_coord = y_coord;
    }

    public int getResId() {
        return resId;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    @Override
    public String toString() {
        return "TableLocationBean{" +
                "locId=" + locId +
                ", tId=" + tId +
                ", cId=" + cId +
                ", x_coord=" + x_coord +
                ", y_coord=" + y_coord +
                ", resId=" + resId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(locId);
        parcel.writeInt(tId);
        parcel.writeInt(cId);
        parcel.writeFloat(x_coord);
        parcel.writeFloat(y_coord);
        parcel.writeInt(resId);
    }
}

