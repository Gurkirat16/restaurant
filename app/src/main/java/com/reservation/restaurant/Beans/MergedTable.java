package com.reservation.restaurant.Beans;

/**
 * Created by mitaly on 15/10/17.
 */

public class MergedTable {
    int mId;
    int tId;
    String tableName;

    public MergedTable() {
    }

    public MergedTable(int mId, int tId, String tableName) {
        this.mId = mId;
        this.tId = tId;
        this.tableName = tableName;
    }

    public int getmId() {
        return mId;
    }

    public void setmId(int mId) {
        this.mId = mId;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "Merged Table{" +
                "mId=" + mId +
                ", tId=" + tId +
                ", tableName='" + tableName + '\'' +
                '}';
    }
}
