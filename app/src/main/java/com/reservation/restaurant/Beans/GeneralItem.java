package com.reservation.restaurant.Beans;

import android.annotation.SuppressLint;
import android.os.Parcel;

import java.util.List;

/**
 * Created by mitaly on 16/8/17.
 */

@SuppressLint("ParcelCreator")
public class GeneralItem  extends ListItem{

    private Reservations rev;

    public Reservations getRev() {
        return rev;
    }

    public void setRev(Reservations rev) {
        this.rev = rev;
    }

    @Override
    public int getType() {
        return TYPE_GENERAL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

    }
}
