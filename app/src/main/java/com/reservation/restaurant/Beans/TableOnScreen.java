package com.reservation.restaurant.Beans;

import java.io.Serializable;

/**
 * Created by gurkirat on 2/10/17.
 */

public class TableOnScreen implements Serializable{

    int tId;
    float x_coord;
    float y_coord;
    String tableName;

    public TableOnScreen(){

    }

    public TableOnScreen(int tId, float x_coord, float y_coord, String tableName) {
        this.tId = tId;
        this.x_coord = x_coord;
        this.y_coord = y_coord;
        this.tableName = tableName;
    }

    public int gettId() {
        return tId;
    }

    public void settId(int tId) {
        this.tId = tId;
    }

    public float getX_coord() {
        return x_coord;
    }

    public void setX_coord(float x_coord) {
        this.x_coord = x_coord;
    }

    public float getY_coord() {
        return y_coord;
    }

    public void setY_coord(float y_coord) {
        this.y_coord = y_coord;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    @Override
    public String toString() {
        return "TableOnScreen{" +
                "tId=" + tId +
                ", x_coord=" + x_coord +
                ", y_coord=" + y_coord +
                ", tableName='" + tableName + '\'' +
                '}';
    }
}
