package com.reservation.restaurant.Beans;

import android.os.Parcelable;

import java.io.Serializable;

/**
 * Created by mitaly on 16/8/17.
 */

public abstract class ListItem implements Parcelable {
    public static final int TYPE_DATE = 0;
    public static final int TYPE_GENERAL = 1;

    abstract public int getType();
}
