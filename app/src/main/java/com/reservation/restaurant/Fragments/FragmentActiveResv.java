package com.reservation.restaurant.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.daimajia.swipe.SwipeLayout;
import com.reservation.restaurant.Activities.ResvDetailsActivity;
import com.reservation.restaurant.Activities.RsvRequestsActivity;
import com.reservation.restaurant.Adapters.ActiveResvAdapter;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentActiveResv extends Fragment implements MyResponse{

    FrameLayout container;
    static Context context;
    ArrayList<Reservations> listActiveResv;
    RecyclerView recViewActiveResv;
    ActiveResvAdapter adapter;
    MyDB myDB;
    Reservations resv;
    int whichRequest, position;
    View emptyStateView;

    public FragmentActiveResv() {
    }

    public static FragmentActiveResv newInstance(ArrayList<Reservations> listActiveResv, Context c) {
        FragmentActiveResv fragment = new FragmentActiveResv();
        Bundle args = new Bundle();
        args.putParcelableArrayList("listActiveResv", listActiveResv);
        context = c;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listActiveResv = getArguments().getParcelableArrayList("listActiveResv");
        }
    }

    public void addNewActiveResv(Reservations resv){
        if(container.findViewById(R.id.llNoActiveResv) != null){
            container.removeView(emptyStateView);
        }
        listActiveResv.add(0, resv);
        adapter.notifyDataSetChanged();
        recViewActiveResv.smoothScrollToPosition(0);
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_active_resv, container, false);
        recViewActiveResv = (RecyclerView)view.findViewById(R.id.recViewActiveResv);
        this.container = view.findViewById(R.id.container);
        adapter = new ActiveResvAdapter(listActiveResv, context, FragmentActiveResv.this);
        recViewActiveResv.setLayoutManager(new LinearLayoutManager(context));
        recViewActiveResv.setItemAnimator(new DefaultItemAnimator());
        recViewActiveResv.setAdapter(adapter);
        if(listActiveResv.isEmpty())
            showEmptyView();
        return view;
    }

    public void showDetailsActivity(int position){
        Intent i = new Intent(context, ResvDetailsActivity.class);
        i.putExtra("resvDetails", (Parcelable) listActiveResv.get(position));
        startActivity(i);
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(this);
        myDB.initRequestQueue(context);

    }

    public void showEmptyView(){
        emptyStateView = getLayoutInflater().inflate(R.layout.empty_active_resv, null, false);
        container.addView(emptyStateView);
    }

    public void actionResvOver(Reservations resv,int position){
        Utility.initDialog(getActivity());
        this.position = position;
        this.resv = resv;
        whichRequest = Utility.ACTION_RESV_OVER;

        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("rCmbId", resv.getrCmbId());
            initMyDB();
            myDB.processRequest(Request.Method.POST, Utility.URL_ACTION_RESV_OVER, jsonObject);
        }catch (JSONException e){
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }
    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");

            switch (whichRequest){
                case Utility.ACTION_RESV_OVER:
                    if(errorCode==0){
                        listActiveResv.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, listActiveResv.size());
                        if(listActiveResv.size() < 0){
                            showEmptyView();
                        }
                    }else{

                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }
}
