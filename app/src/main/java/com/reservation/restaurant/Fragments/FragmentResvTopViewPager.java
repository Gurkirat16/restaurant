package com.reservation.restaurant.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class FragmentResvTopViewPager extends Fragment {

    FrameLayout noResvContainer;
    TabLayout tabLayout;
    ViewPager viewPager;
    ResvCategoriesAdapter adapter;
    ArrayList<Reservations> listResv;
    LinkedHashMap<String, ArrayList<Reservations>> hashMapResv = new LinkedHashMap<>();
    HashMap<String, List<Reservations>> hashMapDateResv;
    ArrayList<ArrayList<Reservations>> wholeListResv;
    ArrayList<Reservations> listResvCategory;
    int curPos;
    FragmentEmptyState fragmentEmptyState;

    public FragmentResvTopViewPager() {
    }
    public static FragmentResvTopViewPager newInstance(ArrayList<Reservations> listResv) {
        FragmentResvTopViewPager fragment = new FragmentResvTopViewPager();
        Bundle args = new Bundle();
        args.putParcelableArrayList("listResv", listResv);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            listResv = getArguments().getParcelableArrayList("listResv");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_resv_top_view_pager, container, false);
        viewPager = view.findViewById(R.id.internalViewPager);
        tabLayout = view.findViewById(R.id.internalTabLayout);
        noResvContainer = view.findViewById(R.id.container);
        return view;
    }

    public void showEmptyView(){
        tabLayout.setVisibility(View.GONE);
        viewPager.setVisibility(View.GONE);
        noResvContainer.setVisibility(View.VISIBLE);
        fragmentEmptyState = FragmentEmptyState.newInstance(R.layout.empty_reservations);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentEmptyState).commit();
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        if(listResv.isEmpty())
            showEmptyView();
        else{
            setHashMapResv();
            adapter = new ResvCategoriesAdapter(getChildFragmentManager());
            viewPager.setAdapter(adapter);
            viewPager.setCurrentItem(0);
            tabLayout.setupWithViewPager(viewPager);
        }
    }

    public void showRecentResv(){
        if(listResv.isEmpty()){
            showEmptyView();
        }else{
            curPos = viewPager.getCurrentItem();
            listResvCategory = wholeListResv.get(curPos);
            Fragment curFrag = adapter.getRegisteredFragment(curPos);
            ((CategoriesResvFragment)curFrag).showRecentResvView(listResvCategory, curFrag);
        }
    }
    public void showDateView(boolean onDateChosen, String chosenDate){
        if(listResv.isEmpty())
            showEmptyView();
        else{
            curPos = viewPager.getCurrentItem();
            listResvCategory = wholeListResv.get(curPos);
            Fragment curFrag = adapter.getRegisteredFragment(curPos);

            hashMapDateResv = new HashMap<>();
            if(!listResv.isEmpty()){
                //group by date
                if(!onDateChosen){
                    for(Reservations rev : listResvCategory){
                        String mapKey = rev.getrDate();
                        //if entry of key/date is already there in Hashmap
                        if(hashMapDateResv.containsKey(mapKey)){
                            hashMapDateResv.get(mapKey).add(rev);
                        }//else we need to create new key
                        else{
                            List<Reservations> list = new ArrayList<>();
                            list.add(rev);
                            hashMapDateResv.put(mapKey, list);
                        }
                    }
                }
                //if user selects particular date
                else{
                    boolean flag = false;
                    for(Reservations rev : listResvCategory){

                        if(rev.getrDate().equals(chosenDate)) {
                            if(!flag){
                                flag = true;
                                List<Reservations> list = new ArrayList<>();
                                list.add(rev);
                                hashMapDateResv.put(chosenDate, list);

                            }else{
                                hashMapDateResv.get(chosenDate).add(rev);
                            }

                        }

                    }
                }
            }
            if(onDateChosen)
                ((CategoriesResvFragment)curFrag).showChooseDateView(hashMapDateResv, curFrag);
            else
                ((CategoriesResvFragment)curFrag).showGroupByDateView(hashMapDateResv, curFrag);
        }
    }

    void setHashMapResv(){
        for(Reservations rev : listResv){
            String category = rev.getrCategoryInfo().getcName();
            if(hashMapResv.containsKey(category)){
                hashMapResv.get(category).add(rev);
            }else{
                ArrayList<Reservations> list = new ArrayList<>();
                if(rev.getrStatus() != -1)
                    list.add(rev);
                hashMapResv.put(category, list);
            }
        }
        wholeListResv = new ArrayList<ArrayList<Reservations>>(hashMapResv.values());
    }
    public class ResvCategoriesAdapter extends FragmentStatePagerAdapter {

        String[] titles;
        SparseArray<Fragment> registeredFragments = new SparseArray<>();
        public ResvCategoriesAdapter(FragmentManager fm){
            super(fm);
            titles = hashMapResv.keySet().toArray(new String[hashMapResv.size()]);
        }
        @Override
        public Fragment getItem(int position) {
            listResvCategory = wholeListResv.get(position);
            Fragment fragment = CategoriesResvFragment.newInstance(listResvCategory);
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return hashMapResv.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        public Fragment getRegisteredFragment(int position){
            return registeredFragments.get(position);
        }
    }

}
