package com.reservation.restaurant.Fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reservation.restaurant.Activities.ResvDetailsActivity;
import com.reservation.restaurant.Activities.RsvRequestsActivity;
import com.reservation.restaurant.Adapters.ReservationsAdapter;
import com.reservation.restaurant.Beans.GeneralItem;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.R;

import java.util.ArrayList;

public class FragmentRecentResv extends Fragment {

    RecyclerView recyclerViewRecent;
    ReservationsAdapter adapter;
    ArrayList consolidatedList;

    public FragmentRecentResv() {

    }

    public static FragmentRecentResv newInstance(Context context) {

        Bundle args = new Bundle();

        FragmentRecentResv fragment = new FragmentRecentResv();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_recent_resv, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recyclerViewRecent = (RecyclerView)view.findViewById(R.id.recViewResvRecent);
        adapter = new ReservationsAdapter(consolidatedList, 0);
        recyclerViewRecent.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerViewRecent.setItemAnimator(new DefaultItemAnimator());
        recyclerViewRecent.setAdapter(adapter);

        recyclerViewRecent.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recyclerViewRecent, new RecyclerItemClickListener.OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                Intent i = new Intent(getContext(), ResvDetailsActivity.class);
                i.putExtra("resvDetails", (Parcelable)((GeneralItem)consolidatedList.get(position)).getRev());
                startActivity(i);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        consolidatedList = getArguments().getParcelableArrayList("consolidatedList");
        Log.i("show", "cons: "+consolidatedList.toString());
    }
}
