package com.reservation.restaurant.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reservation.restaurant.Activities.ResvDetailsActivity;
import com.reservation.restaurant.Adapters.ReservationsAdapter;
import com.reservation.restaurant.Beans.GeneralItem;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.R;

import java.util.ArrayList;


public class FragmentChooseDateResv extends Fragment {
    RecyclerView recViewResvDate;
    ArrayList consolidatedList;
    ReservationsAdapter adapter;

    public FragmentChooseDateResv() {
    }

    public static FragmentChooseDateResv newInstance(String param1, String param2) {
        FragmentChooseDateResv fragment = new FragmentChooseDateResv();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            consolidatedList = getArguments().getParcelableArrayList("consolidatedList");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_choose_date_resv, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        recViewResvDate = (RecyclerView)view.findViewById(R.id.recViewResvDate);
        adapter = new ReservationsAdapter(consolidatedList, 1);
        recViewResvDate.setLayoutManager(new LinearLayoutManager(getActivity()));
        recViewResvDate.setItemAnimator(new DefaultItemAnimator());
        recViewResvDate.setAdapter(adapter);

        recViewResvDate.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), recViewResvDate, new RecyclerItemClickListener.OnItemClickListener(){

            @Override
            public void onItemClick(View view, int position) {
                if(consolidatedList.get(position) instanceof GeneralItem){
                    Intent i = new Intent(getContext(), ResvDetailsActivity.class);
                    i.putExtra("resvDetails", (Parcelable)((GeneralItem)consolidatedList.get(position)).getRev());
                    startActivity(i);
                }
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));
    }
}
