package com.reservation.restaurant.Fragments;

import android.content.Context;

import android.content.Intent;
import android.os.Bundle;

import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.android.volley.Request;
import com.daimajia.swipe.util.Attributes;

import com.reservation.restaurant.Activities.ResvDetailsActivity;
import com.reservation.restaurant.Activities.RsvRequestsActivity;
import com.reservation.restaurant.Adapters.ResvRequestsAdapter;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class FragmentResvRequests extends Fragment implements MyResponse{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    FrameLayout mContainer;
    RecyclerView recViewResvRequests;

    ResvRequestsAdapter adapter;

    ArrayList<Reservations> listResvReq = new ArrayList<>();
    static Context context ;
    MyDB myDB;
    int whichRequest;
    int action, position;
    Reservations resv;
    FragmentEmptyState fragmentEmptyState;
    View emptyStateView;

    public FragmentResvRequests() {
    }

    public static FragmentResvRequests newInstance(ArrayList<Reservations> listResvReq, Context c) {
        FragmentResvRequests fragment = new FragmentResvRequests();
        Bundle args = new Bundle();
        args.putParcelableArrayList("listResvReq", listResvReq);
        context = c;
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            listResvReq = getArguments().getParcelableArrayList("listResvReq");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view =  inflater.inflate(R.layout.fragment_resv_requests, container, false);

        recViewResvRequests = (RecyclerView)view.findViewById(R.id.recViewResvRequests);
        mContainer = view.findViewById(R.id.container);
        adapter = new ResvRequestsAdapter(listResvReq,context, FragmentResvRequests.this);

        recViewResvRequests.setLayoutManager(new LinearLayoutManager(context));
        recViewResvRequests.setItemAnimator(new DefaultItemAnimator());
        recViewResvRequests.setAdapter(adapter);
        adapter.setMode(Attributes.Mode.Single);

        if(listResvReq.isEmpty())
            showEmptyView();

        return view;
    }

    public void showDetailsActivity(int position){
        Intent i = new Intent(context, ResvDetailsActivity.class);
        i.putExtra("resvDetails", (Parcelable) listResvReq.get(position));
        startActivity(i);
    }

    public void showEmptyView(){
        /*recViewResvRequests.setVisibility(View.GONE);
        emptyView.setVisibility(View.VISIBLE);
        fragmentEmptyState = FragmentEmptyState.newInstance(R.layout.empty_resv_req);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentEmptyState).commit();*/
        emptyStateView = getLayoutInflater().inflate(R.layout.empty_resv_req, null, false);
        mContainer.addView(emptyStateView);
    }

    public void actionResvReq(int action, Reservations resv,int position){
        Utility.initDialog(getActivity());
        this.action = action;
        this.position = position;
        this.resv = resv;
        whichRequest = Utility.ACTION_RESV_REQ;

        try{
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("rCmbId", resv.getrCmbId());
            //action-1 accepted,  action-2 rejected
            jsonObject.put("rStatus", action);
            jsonObject.put("cusEmail", resv.getrCusInfo().getCusEmail());
            initMyDB();
            myDB.processRequest(Request.Method.POST, Utility.URL_ACTION_RESV_REQ, jsonObject);
        }catch (JSONException e){
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }
    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(this);
        myDB.initRequestQueue(context);

    }

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");

            switch (whichRequest){
                case Utility.ACTION_RESV_REQ:
                    if(errorCode==0){
                        listResvReq.remove(position);
                        adapter.notifyItemRemoved(position);
                        adapter.notifyItemRangeChanged(position, listResvReq.size());
                        if(listResvReq.size() <= 0){
                            showEmptyView();
                        }
                        //if accepted, add the item to Active Reservation list
                        if(action == 1){
                            ((FragmentActiveResv)(((RsvRequestsActivity)getActivity()).getSecondFragmentReference()))
                                    .addNewActiveResv(resv);
                        }
                    }else{

                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }
}
