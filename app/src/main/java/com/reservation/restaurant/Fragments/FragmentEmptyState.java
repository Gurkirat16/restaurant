package com.reservation.restaurant.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.reservation.restaurant.R;

public class FragmentEmptyState extends Fragment {

    int layout;
    public FragmentEmptyState() {
    }

    public static FragmentEmptyState newInstance(int layout) {
        FragmentEmptyState fragment = new FragmentEmptyState();
        Bundle bundle = new Bundle();
        bundle.putInt("layout", layout);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            layout = getArguments().getInt("layout");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.from(getContext()).inflate(layout, container, false);
        return view;
    }
}
