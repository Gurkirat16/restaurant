package com.reservation.restaurant.Fragments;

import android.content.DialogInterface;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.android.volley.Request;
import com.reservation.restaurant.Adapters.TablesAdapter;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Beans.TableLocationBean;
import com.reservation.restaurant.Beans.TableOnScreen;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.OnDragTouchListener;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import me.grantland.widget.AutofitHelper;

public class FragmentFloormap extends Fragment implements MyResponse{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    AlertDialog dialog;
    RelativeLayout root;
    RelativeLayout.LayoutParams layoutParams;
    public static HashMap<LinearLayout, TableOnScreen> map;
    Button tableState;
    Boolean edit = false;
    int screenHeight, screenWidth;
    ArrayList<Table> tablesOnScreen, tablesOffScreen, mergeableTables;
    ArrayList<TableLocationBean> tableLocationList;
    Bundle bundle;
    TablesAdapter tablesAdapter;
    RecyclerView rvDialogTables;
    Table table;
    float centerX, centerY;
    TableOnScreen newTable, oldTable;
    UserSessionManager sessionManager;
    MyDB myDB;
    int resId, cId;
    int whichRequest;
    ArrayList<Integer> mergeCombination = new ArrayList();
    public static float screenAdjust;
    int densityDpi;
    int rootWidth, rootHeight;

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(getContext());
    }

    public FragmentFloormap() {

    }

    public static FragmentFloormap newInstance(String param1, String param2) {
        FragmentFloormap fragment = new FragmentFloormap();
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        initDB();
        Log.i("show","onResume called");
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Utility.initDialog(getActivity());
        bundle = getArguments();
        //
        sessionManager = new UserSessionManager(getContext());
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        if (getArguments() != null) {
            cId = bundle.getInt("cId");
            tablesOnScreen = bundle.getParcelableArrayList("tablesOnScreen");
            Log.i("show", "in onCreate tablesOnScreen: " + tablesOnScreen.toString());
            tablesOffScreen = bundle.getParcelableArrayList("tablesOffScreen");
            Log.i("show", "in onCreate tablesOffScreen: " + tablesOffScreen.toString());
            tableLocationList = bundle.getParcelableArrayList("tableLocationList");
            Log.i("show", "in onCreate tableLocationList: " + tableLocationList.toString());
            mergeableTables = bundle.getParcelableArrayList("mergeableTables");
            Log.i("show", "in onCreate mergeableTables: " + mergeableTables.toString());
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.content_floormap, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        root = (RelativeLayout) view.findViewById(R.id.root);
        map = new HashMap<>();
        tableState = (Button)view.findViewById(R.id.tableState);

        DisplayMetrics displaymetrics = new DisplayMetrics();
        getActivity().getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        densityDpi = displaymetrics.densityDpi;
        screenAdjust = densityDpi/160f;
        screenHeight = displaymetrics.heightPixels;
//        centerY = screenHeight/2;
        screenWidth = displaymetrics.widthPixels;
//        centerX = screenWidth/2;
        Log.i("show", "height: "+screenHeight);
        Log.i("show", "width: "+screenWidth);
        Log.i("show", "screenAdjust: "+screenAdjust);

        tableState.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edit){
                    edit = true;
                    addListenersOnTables();
                } else {
                    Utility.initDialog(getActivity());
                    edit = false;
                    disableListenersOnTables();
                    whichRequest = Utility.SET_LOCATION_TABLE;
                    handler.sendEmptyMessage(whichRequest);
                }
            }
        });

        ViewTreeObserver vto = root.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                rootWidth  = root.getWidth();
                rootHeight  = root.getHeight();
                if (Build.VERSION.SDK_INT < 16) {
                    root.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    root.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                centerX = rootWidth/2;
                centerY = rootHeight/2;
                Log.i("show", "rootWidth: "+rootWidth);
                Log.i("show", "rootHeight: "+rootHeight);
                viewTablesOnScreen();
            }
        });

        for(LinearLayout table : map.keySet()) {
            table.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    return true;
                }
            });
        }

    }

    void addListenersOnTables(){
        for(LinearLayout table : map.keySet()) {
            table.setOnTouchListener(new OnDragTouchListener(table));
            table.setEnabled(true);
        }
        root.setBackgroundResource(R.drawable.graphs);
        tableState.setText("Save");
    }

    void disableListenersOnTables(){
        for(LinearLayout table : map.keySet()){
            table.setEnabled(false);
        }
        root.setBackgroundResource(0);
        tableState.setText("Edit");
    }

    void addNewTable(){

        /*int viewWidth = (int)getActivity().getResources().getDimension(R.dimen.llTableWidth);
        int viewHeight =(int) getActivity().getResources().getDimension(R.dimen.llTableHeight);
        viewHeight =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewHeight, getResources().getDisplayMetrics());
        viewWidth =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewWidth, getResources().getDisplayMetrics());
*/
        int viewWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
        int viewHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
        layoutParams = new RelativeLayout.LayoutParams(viewWidth, viewHeight);

        LinearLayout llNewTable = new LinearLayout(getActivity());
        llNewTable.setOrientation(LinearLayout.VERTICAL);
        llNewTable.setLayoutParams(layoutParams);
        llNewTable.setId(table.gettId());

        TextView txtTableName = new TextView(getActivity());
        LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        txtParams.setMargins(0, 10, 0, 0);
        txtParams.gravity = Gravity.CENTER;
        txtTableName.setText(table.getTableName());
        txtTableName.setTextColor(getResources().getColor(R.color.light_black));
        if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            txtTableName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
        }
        AutofitHelper.create(txtTableName);
        txtTableName.setLayoutParams(txtParams);

        AppCompatImageView imgTable = new AppCompatImageView(getActivity());
        imgTable.setImageResource(R.drawable.ic_table_floormap);
        LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        imgParams.gravity = Gravity.CENTER_HORIZONTAL;
        imgTable.setLayoutParams(imgParams);

        llNewTable.addView(txtTableName);
        llNewTable.addView(imgTable);

        llNewTable.setX(centerX);
        llNewTable.setY(centerY);

        root.addView(llNewTable);
        Log.i("TAG", "table added");

        llNewTable.setOnTouchListener(new OnDragTouchListener(llNewTable));
        llNewTable.setEnabled(true);
        root.setBackgroundResource(R.drawable.graphs);

        newTable = new TableOnScreen(table.gettId(), centerX, centerY, table.getTableName());

        newTable.setX_coord(centerX/FragmentFloormap.screenAdjust);
        newTable.setY_coord(centerY/FragmentFloormap.screenAdjust);
        map.put(llNewTable, newTable);
        addListenersOnTables();

    }

    void viewTablesOnScreen(){

        for(int i = 0; i < tablesOnScreen.size(); i++){
            Table t = tablesOnScreen.get(i);
            TableLocationBean l = new TableLocationBean();
            for(int j = 0; j < tableLocationList.size(); j++) {
                if (tableLocationList.get(j).gettId() == tablesOnScreen.get(i).gettId()){
                    l = tableLocationList.get(j);
                    break;
                }
            }

            oldTable = new TableOnScreen(t.gettId(), l.getX_coord(), l.getY_coord(), t.getTableName());

           /* int viewWidth = (int)resources.getDimension(R.dimen.llTableWidth);
            int viewHeight =(int) resources.getDimension(R.dimen.llTableHeight);
            viewHeight =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewHeight, getResources().getDisplayMetrics());
            viewWidth =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewWidth, getResources().getDisplayMetrics());*/
            int viewWidth = ViewGroup.LayoutParams.WRAP_CONTENT;
            int viewHeight = ViewGroup.LayoutParams.WRAP_CONTENT;
            layoutParams = new RelativeLayout.LayoutParams(viewWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

            LinearLayout llNewTable = new LinearLayout(getActivity());
            llNewTable.setOrientation(LinearLayout.VERTICAL);
            llNewTable.setLayoutParams(layoutParams);
            llNewTable.setId(t.gettId());

            TextView txtTableName = new TextView(getActivity());
            LinearLayout.LayoutParams txtParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            txtParams.setMargins(0, 10, 0, 0);
            txtParams.gravity = Gravity.CENTER;
            txtTableName.setText(oldTable.getTableName());
            txtTableName.setTextColor(getResources().getColor(R.color.light_black));
            if(android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                txtTableName.setTextAlignment(View.TEXT_ALIGNMENT_CENTER);
            }
            AutofitHelper.create(txtTableName);
            txtTableName.setLayoutParams(txtParams);
/*

            int imgWidth = (int)resources.getDimension(R.dimen.imgTableWidth);
            int imgHeight =  (int)resources.getDimension(R.dimen.imgTableHeight);
            imgWidth =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewHeight, getResources().getDisplayMetrics());
            imgHeight =  (int)TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, viewWidth, getResources().getDisplayMetrics());
*/

            AppCompatImageView imgTable = new AppCompatImageView(getActivity());
            imgTable.setImageResource(R.drawable.ic_table_floormap);
            LinearLayout.LayoutParams imgParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            imgParams.gravity = Gravity.CENTER_HORIZONTAL;
            imgTable.setLayoutParams(imgParams);

            llNewTable.addView(txtTableName);
            llNewTable.addView(imgTable);

        /*    Point size = new Point();
            display.getSize(size);
            llNewTable.measure(size.x, size.y);*/

            Log.i("show","tableName: "+t.getTableName());
            Log.i("show", "viewWidth: "+viewWidth);
            Log.i("show", "viewHeight: "+viewHeight);

            float inputX = (l.getX_coord())*screenAdjust;
            float inputY = (l.getY_coord())*screenAdjust;

            Log.i("show", "inputX old: "+inputX);
            Log.i("show", "inputY old: "+inputY);

            if ((inputX+viewWidth) > rootWidth) {
                inputX = rootWidth - viewWidth;
            }

            if ((inputY+viewHeight) > rootHeight) {
                inputY = rootHeight - viewHeight;
            }

            Log.i("show", "inputX: "+inputX);
            Log.i("show", "inputY: "+inputY);


            llNewTable.setX(inputX);
            llNewTable.setY(inputY);

            root.addView(llNewTable);

            map.put(llNewTable, oldTable);


        }

        Utility.dismissDialog();

    }
    void showEmptyMergeTablesDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("There is no table to add in merge combination.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();

    }

    void showEmptyDialog(){
        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setMessage("All the tables of this category are already placed on the screen.");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builder.create().show();

    }

    public void viewTablesOffScreen(){

        if(tablesOffScreen.isEmpty()){
            showEmptyDialog();
        } else {
            showDialogToChooseTable();
        }

    }
    void showDialogToChooseTable(){

        LayoutInflater inflater = LayoutInflater.from(getContext());
        View content = inflater.inflate(R.layout.custom_dialog_tables, null);

        tablesAdapter = new TablesAdapter(tablesOffScreen, 2);
        rvDialogTables = (RecyclerView) content.findViewById(R.id.rvDialogTables);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        rvDialogTables.setLayoutManager(layoutManager);
        rvDialogTables.setItemAnimator(new DefaultItemAnimator());
        rvDialogTables.setAdapter(tablesAdapter);
        rvDialogTables.addOnItemTouchListener(new RecyclerItemClickListener(getContext(), rvDialogTables, new RecyclerItemClickListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {
                table = tablesOffScreen.get(position);

                tablesOffScreen.remove(position);
                tablesAdapter.notifyDataSetChanged();

                tablesOnScreen.add(table);
                tablesAdapter.notifyDataSetChanged();

                addNewTable();

                dialog.dismiss();
                Log.i("show", "tablesOffScreen after removing: "+tablesOffScreen.toString());
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

        final AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Choose a table: ");
        builder.setView(content);
        builder.setNegativeButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        builder.create();
        dialog = builder.show();

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                JSONObject jsonObject;
                switch (msg.what){
                    case Utility.SET_LOCATION_TABLE:
                        jsonObject = new JSONObject();
                        JSONObject jsonObject1;
                        JSONArray jsonArray = new JSONArray();
                        for(TableOnScreen tableOnScreen : map.values()) {
                            jsonObject1 = new JSONObject();
                            jsonObject1.put("tId", tableOnScreen.gettId());
                            jsonObject1.put("x_coord",tableOnScreen.getX_coord());
                            jsonObject1.put("y_coord",tableOnScreen.getY_coord());
                            jsonObject1.put("tableName",tableOnScreen.getTableName());
                            jsonArray.put(jsonObject1);
                        }
                        jsonObject.put("locationList", jsonArray);
                        jsonObject.put("resId", resId);
                        jsonObject.put("cId", cId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_SET_LOCATION, jsonObject);
                        break;

                    case Utility.MERGE_TABLES:
                        jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        jsonObject.put("cId", cId);
                        JSONArray arr = new JSONArray(mergeCombination);
                        jsonObject.put("mergeCombination", arr);
                        Log.i("show", "obj : "+jsonObject.toString());
                        myDB.processRequest(Request.Method.POST, Utility.URL_MERGE_TABLES, jsonObject);
                        mergeCombination.clear();
                        break;
                }
            }catch (JSONException e){
                e.printStackTrace();
                Utility.dismissDialog();
            }
        }
    };
    @Override
    public void onMyResponse(JSONObject jsonObject) {
        Log.i("show", "jsonObj: "+jsonObject.toString());
        try{
            int errorCode = jsonObject.getInt("error");
            switch (whichRequest){
                case Utility.SET_LOCATION_TABLE:
                    Toast.makeText(getActivity(), jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    Utility.dismissDialog();
                    break;
                case Utility.MERGE_TABLES:
                    if(errorCode == 0){
                        Toast.makeText(getActivity(), "Combination formed!", Toast.LENGTH_SHORT).show();
                        Utility.dismissDialog();
                    }
                    break;

            }
        }catch (JSONException e){
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }

    public void mergeTables(){

        List<String> listItems = new ArrayList<String>();

        if(mergeableTables.size() <= 1){
            showEmptyMergeTablesDialog();
        }else{
            for(int i = 0; i < mergeableTables.size(); i++){
                String tableName = mergeableTables.get(i).getTableName();
                listItems.add(tableName);
            }
            final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

            AlertDialog dialog = new AlertDialog.Builder(getActivity())
                    .setTitle("Select Tables To Merge")
                    .setMultiChoiceItems(items, null, new DialogInterface.OnMultiChoiceClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int indexSelected, boolean isChecked) {
                            int selectedTableId = mergeableTables.get(indexSelected).gettId();
                            if (isChecked) {
                                mergeCombination.add(selectedTableId);
                                Log.i("show", "list mergeCombination: "+mergeCombination.toString());
                            } else if (mergeCombination.contains(selectedTableId)) {
                                mergeCombination.remove(Integer.valueOf(selectedTableId));
                            }
                        }
                    }).setPositiveButton("Merge", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            if(mergeCombination.size() > 1) {
                                Utility.initDialog(getActivity());
                                whichRequest = Utility.MERGE_TABLES;

                                handler.sendEmptyMessage(Utility.MERGE_TABLES);
                            } else {
                                mergeCombination.clear();
                                Toast.makeText(getContext(), "Combination must consist two or more tables.", Toast.LENGTH_SHORT).show();
                                dialog.dismiss();
                            }
                        }
                    }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int id) {
                            mergeCombination.clear();
                            dialog.dismiss();
                        }
                    }).create();
            dialog.show();
        }
    }

}
