package com.reservation.restaurant.Fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.reservation.restaurant.Beans.DateItem;
import com.reservation.restaurant.Beans.GeneralItem;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CategoriesResvFragment extends Fragment {

    FrameLayout container;
    private ArrayList consolidatedList = new ArrayList();
    private ArrayList<Reservations> listResv = new ArrayList();
    HashMap<String, List<Reservations>> hashMapReservations;
    Fragment curFragment;
    FragmentRecentResv fragmentRecentResv;
    FragmentSortDateResv fragmentSortDateResv;
    FragmentChooseDateResv fragmentChooseDateResv;
    FragmentEmptyState fragmentEmptyState;

    public CategoriesResvFragment() {
    }


    public static CategoriesResvFragment newInstance(ArrayList listResv) {
        CategoriesResvFragment fragment = new CategoriesResvFragment();
        Bundle args = new Bundle();
        args.putParcelableArrayList("listResv", listResv);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle bundle = getArguments();
        if (bundle != null) {
            listResv = bundle.getParcelableArrayList("listResv");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_categories_resv, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        Log.i("show","cons: list:"+listResv.toString());

        if(!listResv.isEmpty()){
            createConsolidatedListRecent();

            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("consolidatedList",consolidatedList);
            fragmentRecentResv = new FragmentRecentResv();
            fragmentRecentResv.setArguments(bundle);
            getChildFragmentManager().beginTransaction()
                    .replace(R.id.container, fragmentRecentResv)
                    .commit();
        }else{
            showEmptyView();
        }
    }

    void createConsolidatedListRecent(){
        consolidatedList = new ArrayList();
        for(Reservations rev : listResv){
            GeneralItem generalItem = new GeneralItem();
            generalItem.setRev(rev);
            consolidatedList.add(generalItem);
        }
    }

    void createConsolidatedList(){
        consolidatedList = new ArrayList();

        //storing date and related reservations together in consolidatedList
        for(String date : hashMapReservations.keySet()){

            DateItem dateItem = new DateItem();
            dateItem.setDate(date);
            consolidatedList.add(dateItem);

            for(Reservations rev : hashMapReservations.get(date)){
                GeneralItem generalItem = new GeneralItem();
                generalItem.setRev(rev);
                consolidatedList.add(generalItem);
            }
        }
    }
    public void showGroupByDateView(HashMap<String, List<Reservations>> hashMapReservations, Fragment curFragment){
        this.hashMapReservations = hashMapReservations;
        createConsolidatedList();
        if(!consolidatedList.isEmpty()){
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("consolidatedList",consolidatedList);
            fragmentSortDateResv = new FragmentSortDateResv();
            fragmentSortDateResv.setArguments(bundle);
            curFragment.getChildFragmentManager().beginTransaction()
                    .replace(R.id.container, fragmentSortDateResv).commit();
        }else{
            showEmptyView(curFragment);
        }
    }

    public void showChooseDateView(HashMap<String, List<Reservations>> hashMapReservations, Fragment curFragment){
        this.hashMapReservations = hashMapReservations;
        createConsolidatedList();
        if(!consolidatedList.isEmpty()){
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("consolidatedList",consolidatedList);
            fragmentChooseDateResv = new FragmentChooseDateResv();
            fragmentChooseDateResv.setArguments(bundle);
            curFragment.getChildFragmentManager().beginTransaction()
                    .replace(R.id.container, fragmentChooseDateResv).commit();
        }else{
            showEmptyView(curFragment);
        }
    }
    public void showRecentResvView(ArrayList listResv, Fragment curFragment){
        this.listResv = listResv;
        createConsolidatedListRecent();
        if(!consolidatedList.isEmpty()){
            Bundle bundle = new Bundle();
            bundle.putParcelableArrayList("consolidatedList",consolidatedList);
            fragmentRecentResv = new FragmentRecentResv();
            fragmentRecentResv.setArguments(bundle);
            curFragment.getChildFragmentManager().beginTransaction()
                    .replace(R.id.container, fragmentRecentResv).commit();
        }else{
            showEmptyView(curFragment);
        }
    }

    public void showEmptyView(Fragment curFragment){
        fragmentEmptyState = FragmentEmptyState.newInstance(R.layout.empty_reservations);
        curFragment.getChildFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentEmptyState).commit();
    }
    public void showEmptyView(){
        fragmentEmptyState = FragmentEmptyState.newInstance(R.layout.empty_reservations);
        getChildFragmentManager().beginTransaction()
                .replace(R.id.container, fragmentEmptyState).commit();
    }
}