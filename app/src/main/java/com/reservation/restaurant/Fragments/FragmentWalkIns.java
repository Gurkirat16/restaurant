package com.reservation.restaurant.Fragments;

import android.app.Dialog;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.google.android.flexbox.FlexboxLayout;
import com.reservation.restaurant.R;

import java.util.ArrayList;

public class FragmentWalkIns extends BottomSheetDialogFragment {

    ArrayList<String> selectedTables;
    ArrayList<String> availableTables = new ArrayList<>();

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        availableTables = getArguments().getStringArrayList("availableTables");
        Log.i("show", "in onCreate");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.i("show", "in onCreateView");
        selectedTables = new ArrayList<>();
        Log.i("show", "in frag: "+availableTables.toString());
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        Log.i("show", "in setUpDialog");
        super.setupDialog(dialog, style);
        View contentView = View.inflate(getContext(), R.layout.fragment_walk_ins, null);
        FlexboxLayout fbl = (FlexboxLayout) contentView.findViewById(R.id.fbl);
        inflateLayoutWithFilters(fbl);
        dialog.setContentView(contentView);
        Log.i("show", "after setContentView");
        contentView.findViewById(R.id.btn_reserve_tbl).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.i("show", "in onClck btn reserve table");
//                closeFilter(selectedTables);
            }
        });

    }

    private void inflateLayoutWithFilters(FlexboxLayout fbl) {
        Log.i("show", "in inflateLayoutWithFilters");

        for (int i = 0; i < availableTables.size(); i++) {
            Log.i("show", "in for Loop fragment");
            View subchild = getActivity().getLayoutInflater().inflate(R.layout.single_chip, null);
            final TextView tv = ((TextView) subchild.findViewById(R.id.txt_title));
            tv.setText(availableTables.get(i));
            tv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (tv.getTag() != null && tv.getTag().equals("selected")) {
                        tv.setTag("unselected");
                        tv.setBackgroundResource(R.drawable.chip_unselected);
                        tv.setTextColor(ContextCompat.getColor(getContext(), R.color.colorPrimary));
                        selectedTables.remove(tv.getText().toString().trim());
                    } else {
                        tv.setTag("selected");
                        tv.setBackgroundResource(R.drawable.chip_selected);
                        tv.setTextColor(ContextCompat.getColor(getContext(), R.color.colorWhite));
                        selectedTables.add(tv.getText().toString().trim());
                    }
                }
            });

            fbl.addView(subchild);
        }

    }
}
