package com.reservation.restaurant.fcm;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.reservation.restaurant.Activities.RsvRequestsActivity;
import com.reservation.restaurant.R;

import org.json.JSONObject;

public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "show";

    //Do not write toast in any method

    public void onMessageReceived(RemoteMessage remoteMessage) {

        if (remoteMessage == null){

            return;
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {

        }
        if (remoteMessage.getData().size() > 0) {

            try {
                JSONObject json = new JSONObject(remoteMessage.getData());
                handleDataMessage(json);
            } catch (Exception e) {
            }
        }
    }

    private void handleDataMessage(JSONObject json) {
        try {
            String message = json.getString("cusName");

            NotificationCompat.Builder notificationBuilder;
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

            if(Build.VERSION.SDK_INT >= 26){
                NotificationChannel channel = new NotificationChannel("resvReq", "ReservationReq",
                        NotificationManager.IMPORTANCE_HIGH);
                notificationManager.createNotificationChannel(channel);
                notificationBuilder = new NotificationCompat.Builder(this, "resvReq");
            }else{
                notificationBuilder = new NotificationCompat.Builder(this);
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, new Intent(
                    this, RsvRequestsActivity.class), 0);

            notificationBuilder
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Reservation Request")
            .setContentText(message)
            .setContentIntent(pendingIntent);

            notificationBuilder.setAutoCancel(true);
            notificationManager.notify(101, notificationBuilder.build());
        }catch (Exception e){
            e.printStackTrace();
        }
    }
}
