package com.reservation.restaurant.Activities;

import android.content.DialogInterface;
import android.content.Intent;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Window;
import android.view.WindowManager;
import com.reservation.restaurant.R;

import com.reservation.restaurant.Helper.Utility;


import static com.reservation.restaurant.Helper.Utility.isNetworkConnected;

public class SplashActivity extends AppCompatActivity{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_splash_screen);

        checkNetwork();
    }

    void checkNetwork(){

        if(isNetworkConnected(this)) {

            if (getSharedPreferences(Utility.SHARED_PREF_NAME, 0).getBoolean(Utility.IS_USER_LOGIN, false)) {
                Intent intent = new Intent(SplashActivity.this, DashboardActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            } else {
                Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }
        } else {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Not connected to internet. Connect now?");
            builder.setCancelable(false);
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                    startActivityForResult(intent, Utility.REQ_CODE_CHECK_INTERNET);
                }
            });
            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                    moveTaskToBack(true);
                }
            });
            builder.create().show();
        }

    }

//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        if (requestCode == Utility.REQ_CODE_CHECK_INTERNET){
//            checkNetwork();
//        }
//    }


    @Override
    protected void onResume() {
        super.onResume();
        checkNetwork();
    }
}
