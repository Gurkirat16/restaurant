package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Adapters.MergeCombnAdapter;
import com.reservation.restaurant.Beans.MergeCmb;
import com.reservation.restaurant.Beans.MergedTable;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MergeCmbActivity extends AppCompatActivity implements MyResponse{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }
    @BindView(R.id.rcvMergeCmb)
    RecyclerView rcvMergeCmb;
    @BindView(R.id.llNoMergeCmb)
    LinearLayout llNoMergeCmb;
    int whichRequest;
    Context context = MergeCmbActivity.this;
    UserSessionManager sessionManager;
    int resId, cId;
    MyDB myDB;
    Dialog pd;
    MergeCombnAdapter mergeCombnAdapter;
    int cmbId, pos, mIdDel, grpPosClicked, childPosClicked;
    ArrayList<MergeCmb> listAllCmb = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_merge_cmb);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Merge Combinations");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        sessionManager = new UserSessionManager(context);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));
        cId = getIntent().getIntExtra("cId", 0);

        initDB();

        pd = Utility.initDialog(context);
        whichRequest = Utility.VIEW_MERGE_COMBN;
        handler.sendEmptyMessage(whichRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                JSONObject jsonObject;
                switch (whichRequest){
                    case Utility.VIEW_MERGE_COMBN:
                        jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        jsonObject.put("cId", cId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_MERGE_COMBN, jsonObject);
                        break;
                    case Utility.DELETE_MERGE_CMB:
                        jsonObject = new JSONObject();
                        jsonObject.put("cmbId", cmbId);
                        Log.i("show", "cmbId del:"+cmbId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_DELETE_MERGE_CMB, jsonObject);
                        break;
                    case Utility.DELETE_MERGED_TABLE:
                        jsonObject = new JSONObject();
                        jsonObject.put("mId", mIdDel);
                        myDB.processRequest(Request.Method.POST, Utility.URL_DELETE_MERGED_TABLE, jsonObject);
                        break;
                }
            }catch (JSONException e){
                e.printStackTrace();
            }
        }
    };

    public void deleteMergeCmb(int grpPos){
        grpPosClicked = grpPos;
        cmbId = listAllCmb.get(grpPos).getCmbId();

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("Are you sure?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                pd = Utility.initDialog(context);
                whichRequest = Utility.DELETE_MERGE_CMB;
                handler.sendEmptyMessage(whichRequest);
            }
        });
        builder.setNegativeButton("Cancel",null);
        builder.create().show();
    }

    public void deleteMergedTable(int childPos, int grpPos){
        grpPosClicked = grpPos;
        cmbId = listAllCmb.get(grpPos).getCmbId();
        childPosClicked = childPos;

        MergeCmb mergeCmb = listAllCmb.get(grpPos);
        MergedTable table = mergeCmb.getList().get(childPos);
        mIdDel = table.getmId();

        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        String items[] = {"Delete"};
        builder.setItems(items, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        switch (i){
                            case 0:
                                pd = Utility.initDialog(context);
                                whichRequest = Utility.DELETE_MERGED_TABLE;
                                handler.sendEmptyMessage(whichRequest);
                                break;
                        }
                    }
                });
                builder.create();
        builder.show();
    }
    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");
            switch (whichRequest){
                case Utility.VIEW_MERGE_COMBN:
                    if(errorCode == 0){
                        JSONArray mergeCmb = jsonObject.getJSONArray("mergeCmb");

                        for(int i =0 ;i<mergeCmb.length(); i++){
                            JSONArray oneMergeCmb = mergeCmb.getJSONArray(i);
                            ArrayList listSingleCmb = new ArrayList();
                            int cmbId = 0;

                            for(int j=0; j<oneMergeCmb.length(); j++){
                                JSONObject mergedTableJson = oneMergeCmb.getJSONObject(j);

                                cmbId = mergedTableJson.getInt("cmbId");
                                int mId = mergedTableJson.getInt("mId");

                                GsonBuilder gsonBuilder = new GsonBuilder();
                                gsonBuilder.serializeNulls();
                                Gson gson = gsonBuilder.create();
                                Table table = gson.fromJson(mergedTableJson.getJSONObject("tableInfo").toString(), Table.class);

                                MergedTable mergedTable = new MergedTable(mId, table.gettId(), table.getTableName());
                                listSingleCmb.add(mergedTable);
                            }
                            listAllCmb.add(new MergeCmb(listSingleCmb, cmbId));
                        }

                        Log.i("show", "list all cmb: "+listAllCmb.toString());

                        mergeCombnAdapter = new MergeCombnAdapter(listAllCmb, MergeCmbActivity.this);

                        rcvMergeCmb.setLayoutManager(new LinearLayoutManager(context));
                        rcvMergeCmb.setItemAnimator(new DefaultItemAnimator());
                        rcvMergeCmb.setAdapter(mergeCombnAdapter);
                    }else{
                        llNoMergeCmb.setVisibility(View.VISIBLE);
                        rcvMergeCmb.setVisibility(View.GONE);
                    }
                    Utility.dismissDialog();
                    break;
                case Utility.DELETE_MERGE_CMB:
                    if(errorCode == 0){
                        listAllCmb.remove(grpPosClicked);
                        mergeCombnAdapter.notifyItemRemoved(grpPosClicked);
                        mergeCombnAdapter.notifyItemRangeChanged(grpPosClicked,listAllCmb.size());
                        if(listAllCmb.isEmpty()){
                            llNoMergeCmb.setVisibility(View.VISIBLE);
                            rcvMergeCmb.setVisibility(View.GONE);
                        }
                    }
                    Utility.dismissDialog();
                    break;
                case Utility.DELETE_MERGED_TABLE:
                    if(errorCode == 0){
                        Log.i("show", "list before: "+ listAllCmb.get(grpPosClicked).getList());
                        listAllCmb.get(grpPosClicked).getList().remove(childPosClicked);
                        Log.i("show", "list after: "+ listAllCmb.get(grpPosClicked).getList());
                        mergeCombnAdapter.notifyChildAdapter(childPosClicked);
                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
    }
}
