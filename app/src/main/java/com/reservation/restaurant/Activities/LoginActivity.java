package com.reservation.restaurant.Activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Beans.AdminBean;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONException;
import org.json.JSONObject;


import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends AppCompatActivity implements MyResponse {

    @BindView(R.id.eTxtEmail)
    EditText eTxtEmail;
    @BindView(R.id.eTxtPassword)
    EditText eTxtPassword;
    @BindView(R.id.btnLogin)
    Button btnLogin;

    Context context = LoginActivity.this;
    MyDB myDB;
    String email, password;
    UserSessionManager sessionManager;

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        ButterKnife.bind(this);
        initDB();

        sessionManager = new UserSessionManager(LoginActivity.this);

        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                email = eTxtEmail.getText().toString().trim();
                password = eTxtPassword.getText().toString().trim();

                if(validate()){
                    Utility.initDialog(context);
                    JSONObject jsonObject = new JSONObject();
                    try {
                        String token = FirebaseInstanceId.getInstance().getToken();
                        jsonObject.put("aEmail", email);
                        jsonObject.put("aPassword", password);
                        jsonObject.put("aToken", token);
                        if(token!=null)
                            myDB.processRequest(Request.Method.POST, Utility.URL_LOGIN_GET_ADMIN, jsonObject);
                        else {
                            Toast.makeText(LoginActivity.this, "Some Error", Toast.LENGTH_SHORT).show();
                            Utility.dismissDialog();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Utility.dismissDialog();
                    }

                }else{
                    Toast.makeText(LoginActivity.this, "Invalid credentials", Toast.LENGTH_SHORT).show();

                }

            }
        });
    }

    boolean validate(){

        boolean valid=true;
        if(!(email.matches("[a-zA-Z0-9_.-]+@[a-z]+\\.+[a-z]+"))){
            valid=false;
            eTxtEmail.setError("Invalid email");
        }
        if(password.length()<4){
            valid=false;
            eTxtPassword.setError("Invalid Password");
        }
        Log.i("test","val - "+valid);
        return valid;
    }

    @Override
    public void onMyResponse(JSONObject jsonObject) {

        try {
            if(jsonObject.getInt("error")==0){

                JSONObject object  = jsonObject.getJSONObject("result");
                Log.i("show", object.toString());
                Gson gson = new GsonBuilder().create();
                AdminBean admin = gson.fromJson(String.valueOf(object), AdminBean.class);

                sessionManager.createLoginSession(admin);

                Utility.dismissDialog();
                Intent i = new Intent(LoginActivity.this, DashboardActivity.class);
                startActivity(i);
                finish();

            } else {
                Toast.makeText(LoginActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
            }
            Utility.dismissDialog();
        } catch (JSONException e) {
            e.printStackTrace();
            Utility.dismissDialog();
        }

    }
}