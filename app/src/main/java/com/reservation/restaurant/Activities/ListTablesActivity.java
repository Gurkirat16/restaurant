package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.daimajia.swipe.SwipeLayout;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Adapters.ListTablesAdapter;
import com.reservation.restaurant.Adapters.TablesAdapter;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import com.suke.widget.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ListTablesActivity extends AppCompatActivity implements MyResponse{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.recyclerTables)
    RecyclerView recyclerViewTables;
    @BindView(R.id.llNoTables)
    LinearLayout llNoTables;

    ListTablesAdapter tablesAdapter;
    int cId, resId, tId, position;
    MyDB myDB;
    UserSessionManager sessionManager;
    JSONArray array;
    ArrayList<Table> tableObject = new ArrayList<>();
    int whichReq = Utility.DELETE_TABLE;
    Table updatedTableObj;
    TextView txtUpdateError;
    Dialog dialogUpdateTable;
    Context context = ListTablesActivity.this;
    SwipeLayout swipeLayout;

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_tables);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Tables List");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        Utility.initDialog(context);
        initDB();

        sessionManager = new UserSessionManager(this);
        cId = getIntent().getIntExtra("cId", 0);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));


        tablesAdapter = new ListTablesAdapter(context, tableObject);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(ListTablesActivity.this);
        recyclerViewTables.setLayoutManager(layoutManager);
        recyclerViewTables.setItemAnimator(new DefaultItemAnimator());
        recyclerViewTables.setAdapter(tablesAdapter);

        handler.sendEmptyMessage(Utility.GET_TABLES);

//        recyclerViewTables.addOnItemTouchListener(new RecyclerItemClickListener(getApplicationContext(), recyclerViewTables, new RecyclerItemClickListener.OnItemClickListener(){
//
//            @Override
//            public void onItemClick(View view, int position) {
//
//                ListTablesActivity.this.position = position;
//                ListTablesActivity.this.tId = tableObject.get(position).gettId();
//
//                AlertDialog.Builder builder = new AlertDialog.Builder(ListTablesActivity.this);
//                String[] items = {"View Table","Delete Table"};
//                builder.setItems(items, new DialogInterface.OnClickListener() {
//                    @Override
//                    public void onClick(DialogInterface dialogInterface, int i) {
//                        switch (i){
//                            case 0:
//                                dialogUpdateTable = new Dialog(ListTablesActivity.this);
//                                dialogUpdateTable.requestWindowFeature(Window.FEATURE_NO_TITLE);
//                                dialogUpdateTable.setContentView(R.layout.dialog_update_table);
//                                dialogUpdateTable.show();
//                                updateTable(tId, dialogUpdateTable);
//                                break;
//                            case 1:
//                                AlertDialog.Builder builder = new AlertDialog.Builder(ListTablesActivity.this);
//                                builder.setTitle("Delete");
//                                builder.setMessage("Are you Sure ?");
//                                builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        Utility.initDialog(context);
//                                        handler.sendEmptyMessage(Utility.DELETE_TABLE);
//                                    }
//                                });
//                                builder.setNegativeButton("Cancel",null);
//                                builder.create().show();
//                                break;
//                        }
//                    }
//                });
//                builder.create().show();
//            }
//
//            @Override
//            public void onLongItemClick(View view, int position) {
//
//            }
//        }));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    /*public void deleteTable(int position, SwipeLayout swipeLayout){
        ListTablesActivity.this.swipeLayout = swipeLayout;
        ListTablesActivity.this.position = position;
        ListTablesActivity.this.tId = tableObject.get(position).gettId();
        AlertDialog.Builder builder = new AlertDialog.Builder(ListTablesActivity.this);
        builder.setTitle("Delete");
        builder.setMessage("Are you Sure ?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                Utility.initDialog(context);
                handler.sendEmptyMessage(Utility.DELETE_TABLE);
            }
        });
        builder.setNegativeButton("Cancel",null);
        builder.create().show();
    }
*/
    public void editTable(final int position){
        ListTablesActivity.this.position = position;
        ListTablesActivity.this.tId = tableObject.get(position).gettId();

        dialogUpdateTable = new Dialog(ListTablesActivity.this);
        dialogUpdateTable.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogUpdateTable.setContentView(R.layout.dialog_update_table);
        dialogUpdateTable.show();

        final EditText etxtTableName = (EditText) dialogUpdateTable.findViewById(R.id.dialogTableName);
        final EditText etxtNumChairs = (EditText) dialogUpdateTable.findViewById(R.id.dialogNumChairs);
        final SwitchButton sbStatusActive = (SwitchButton) dialogUpdateTable.findViewById(R.id.switch_button1);
        final SwitchButton sbMergeable = (SwitchButton) dialogUpdateTable.findViewById(R.id.switch_button);
        final Button updateTable = (Button) dialogUpdateTable.findViewById(R.id.updateTable);
        txtUpdateError = dialogUpdateTable.findViewById(R.id.txtUpdateError);
        //setting the current content of table
        etxtTableName.setText(tableObject.get(position).getTableName());
        etxtNumChairs.setText(String.valueOf(tableObject.get(position).getNumChairs()));
        if(tableObject.get(position).getStatusActive() == 1){
            sbStatusActive.setChecked(true);
        }
        if(tableObject.get(position).getStatusMergeable() == 1){
            sbMergeable.setChecked(true);
        }

        updateTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            boolean isValid = true;
            final String tableName = etxtTableName.getText().toString().trim();
            int numChairs = 0;
            final int statusActive, statusMergeable;
            if(tableName.length() <= 0){
                etxtTableName.setError("Field can't be empty");
                isValid = false;
            }
            if(etxtNumChairs.getText().toString().trim().length() <= 0){
                etxtNumChairs.setError("Field can't be empty");
                isValid = false;
            }else{
                numChairs = Integer.parseInt(etxtNumChairs.getText().toString().trim());
                if(numChairs == 0){
                    etxtNumChairs.setError("Can't be zero");
                    isValid = false;
                }
            }

            if(isValid){
                if(sbStatusActive.isChecked()){
                    statusActive = 1;
                } else {
                    statusActive = 0;
                }
                if(sbMergeable.isChecked()){
                    statusMergeable = 1;
                } else {
                    statusMergeable = 0;
                }

                if((statusActive != tableObject.get(position
                    ).getStatusActive()) && (statusActive == 0)){

                    dialogUpdateTable.dismiss();

                    AlertDialog.Builder builder = new AlertDialog.Builder(ListTablesActivity.this);
                    builder.setMessage("This table may be reserved. \nDo you want to check the active reservations?");
                    builder.setPositiveButton("Show", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            Intent intent = new Intent(ListTablesActivity.this, RsvRequestsActivity.class);
                            intent.putExtra("isFromTablesActivity", true);
                            startActivity(intent);
                        }
                    });
                    builder.setNegativeButton("Update Anyway", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Utility.initDialog(context);
                            updatedTableObj = new Table(tId, cId, tableName, Integer.parseInt(etxtNumChairs.getText().toString().trim()), statusMergeable, statusActive);
                            handler.sendEmptyMessage(Utility.UPDATE_TABLE);
                        }
                    });
                    builder.create().show();
                }else{
                    Utility.initDialog(context);
                    updatedTableObj = new Table(tId, cId, tableName, numChairs, statusMergeable, statusActive);
                    handler.sendEmptyMessage(Utility.UPDATE_TABLE);
                }
            }
            }
        });

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                JSONObject jsonObject;
                switch (msg.what){
                    case Utility.GET_TABLES:
                        jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        jsonObject.put("cId", cId);
                        whichReq = Utility.GET_TABLES;
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_TABLES, jsonObject);
                        break;
                    case Utility.DELETE_TABLE:
                        jsonObject = new JSONObject();
                        Log.i("show", "tId: "+tId);
                        jsonObject.put("tId", tId);
                        whichReq = Utility.DELETE_TABLE;
                        myDB.processRequest(Request.Method.POST, Utility.URL_DELETE_TABLE, jsonObject);
                        break;
                    case Utility.UPDATE_TABLE:
                        jsonObject = new JSONObject();
                        jsonObject.put("tId", updatedTableObj.gettId());
                        jsonObject.put("resId", resId);
                        jsonObject.put("cId", cId);
                        jsonObject.put("tableName",updatedTableObj.getTableName());
                        jsonObject.put("numChairs",updatedTableObj.getNumChairs());
                        jsonObject.put("statusActive",updatedTableObj.getStatusActive());
                        jsonObject.put("statusMergeable",updatedTableObj.getStatusMergeable());
                        whichReq = Utility.UPDATE_TABLE;
                        myDB.processRequest(Request.Method.POST, Utility.URL_UPDATE_TABLE,jsonObject);
                        break;
                }
            }catch (JSONException e) {
                e.printStackTrace();
                Utility.dismissDialog();
            }
        }
    };

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");

            switch (whichReq){
                case Utility.GET_TABLES:
                    if (errorCode == 0) {
                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.serializeNulls();
                        Gson gson = gsonBuilder.create();
                        array = jsonObject.getJSONArray("tables");
                        tableObject.addAll(Arrays.asList(gson.fromJson(String.valueOf(array), Table[].class)));
                        tablesAdapter.notifyDataSetChanged();
                    } else if(errorCode==2){
                        recyclerViewTables.setVisibility(View.GONE);
                        llNoTables.setVisibility(View.VISIBLE);
                    }
                    Utility.dismissDialog();
                    break;
                case Utility.DELETE_TABLE:
                    if (errorCode == 0){
                        Toast.makeText(ListTablesActivity.this, tableObject.get(position).getTableName() + " deleted", Toast.LENGTH_SHORT).show();
                        tablesAdapter.mItemManger.removeShownLayouts(swipeLayout);
                        tablesAdapter.mItemManger.closeAllItems();
                        tableObject.remove(position);
                        tablesAdapter.notifyItemRemoved(position);
                        tablesAdapter.notifyItemRangeChanged(position, tableObject.size());
                        if (tableObject.isEmpty()){
                            recyclerViewTables.setVisibility(View.GONE);
                            llNoTables.setVisibility(View.VISIBLE);
                        }
                    } else if(jsonObject.getString("msg").startsWith("SQLSTATE[23000]")) {
                        Toast.makeText(ListTablesActivity.this, "Can't delete! Table already reserved!", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(ListTablesActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissDialog();
                    break;
                case Utility.UPDATE_TABLE:
                    if (errorCode == 0){
                        dialogUpdateTable.dismiss();
                        Toast.makeText(ListTablesActivity.this, tableObject.get(position).getTableName() + " updated", Toast.LENGTH_SHORT).show();
                        tableObject.set(position, updatedTableObj);
                        tablesAdapter.notifyDataSetChanged();
                    }else if(errorCode == 2){
                        txtUpdateError.setVisibility(View.VISIBLE);
                        txtUpdateError.setText("This table name already exists");
                    }else{
                        dialogUpdateTable.dismiss();
                        Toast.makeText(ListTablesActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        finally {
            Utility.dismissDialog();
        }

    }

}