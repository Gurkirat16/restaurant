package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Adapters.GlobalCategoriesAdapter;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class SelectCategoryActivity extends AppCompatActivity implements MyResponse{

    ArrayList<String> globalCategories = new ArrayList<>();
    GlobalCategoriesAdapter adapter;
    MyDB myDB;

    @BindView(R.id.rvGlobalCategories)
    RecyclerView rvGlobalCategories;
    @BindView(R.id.btnNext)
    Button btnNext;
    ArrayList<String> categoriesChosen = new ArrayList<>();
    int whichRequest;
    int resId;
    boolean firstTime;
    Context context = SelectCategoryActivity.this;
    Dialog pd;
    private boolean selectCategoryVisible = false;

    @Override
    protected void onStart() {
        super.onStart();
        selectCategoryVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        selectCategoryVisible = false;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("show", "jdkfj0");
        setContentView(R.layout.activity_select_category);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        setSupportActionBar(mToolbar);

        ButterKnife.bind(this);

        initMyDB();

        UserSessionManager sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        Intent rcvIntent = getIntent();

        if(rcvIntent.hasExtra("globalCategories")){
            globalCategories = rcvIntent.getStringArrayListExtra("globalCategories");
            adapter = new GlobalCategoriesAdapter(globalCategories);

            rvGlobalCategories.setLayoutManager(new LinearLayoutManager(this));
            rvGlobalCategories.setItemAnimator(new DefaultItemAnimator());
            rvGlobalCategories.setAdapter(adapter);
        }else{
            firstTime = true;
            whichRequest = Utility.GET_GLOBAL_CATEGORIES_FIRST_TIME;
            //opening for first time
            pd = Utility.initDialog(context);
            pd.setOnKeyListener(keyListener);
            if(selectCategoryVisible)
                handler.sendEmptyMessage(whichRequest);
        }

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pd = Utility.initDialog(context);
                pd.setOnKeyListener(keyListener);
                whichRequest = Utility.ADD_NEW_CATEGORY_GLOBAL_LIST;
                handler.sendEmptyMessage(whichRequest);
            }
        });
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(this);
        myDB.initRequestQueue(SelectCategoryActivity.this);

    }
    @Override
    protected void onResume() {
        super.onResume();
        initMyDB();
    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK){
                if(whichRequest ==   Utility.GET_GLOBAL_CATEGORIES_FIRST_TIME){
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                JSONObject jsonObject;
                switch (msg.what){
                    case Utility.GET_GLOBAL_CATEGORIES_FIRST_TIME:
                        if(selectCategoryVisible){
                            jsonObject = new JSONObject();
                            jsonObject.put("resId", resId);
                            jsonObject.put("firstTime", true);
                            if(selectCategoryVisible)
                                myDB.processRequest(Request.Method.POST, Utility.URL_GET_GLOBAL_CATEGORIES, jsonObject);
                        }
                        break;
                    case Utility.ADD_NEW_CATEGORY_GLOBAL_LIST:
                        categoriesChosen = adapter.getCategoriesList();
                        jsonObject = new JSONObject();
                        JSONArray jsonArray = new JSONArray(categoriesChosen);

                        if(jsonArray.length() > 0){
                            jsonObject.put("categories", jsonArray);
                            jsonObject.put("resId", resId);
                            jsonObject.put("singleCategory", 0);
                            myDB.processRequest(Request.Method.POST, Utility.URL_ADD_CATEGORIES, jsonObject);

                        }else{
                            Intent i = new Intent(SelectCategoryActivity.this, CreateCategoryActivity.class);
                            finish();
                            startActivity(i);
                        }
                        break;

                }
            }catch (JSONException e){
                e.printStackTrace();
                Utility.dismissDialog();
            }
        }
    };
    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");

            switch (whichRequest){
                case Utility.GET_GLOBAL_CATEGORIES_FIRST_TIME:
                    if(selectCategoryVisible){
                        if(errorCode == 0){
                            JSONArray categoriesJsonArray = jsonObject.getJSONArray("categories");
                            for(int i = 0;i<categoriesJsonArray.length();i++)
                                globalCategories.add((String)categoriesJsonArray.get(i));

                            adapter = new GlobalCategoriesAdapter(globalCategories);

                            rvGlobalCategories.setLayoutManager(new LinearLayoutManager(this));
                            rvGlobalCategories.setItemAnimator(new DefaultItemAnimator());
                            rvGlobalCategories.setAdapter(adapter);
                        }
                        Utility.dismissDialog();
                    }
                    break;
                case Utility.ADD_NEW_CATEGORY_GLOBAL_LIST:
                    if(errorCode == 0){
                        if(firstTime){
                            Intent i = new Intent(SelectCategoryActivity.this, CreateCategoryActivity.class);
                            finish();
                            startActivity(i);
                        }else{
                            Toast.makeText(SelectCategoryActivity.this, "Categories added", Toast.LENGTH_LONG).show();

                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            ArrayList<TableCategoryBean> list = new ArrayList<>();
                            list.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonObject.getJSONArray("categoriesInserted")), TableCategoryBean[].class)));
                            Log.i("show", "categories added");
                            Intent intent = new Intent();
                            intent.putParcelableArrayListExtra("categoriesAdded", list);
                            setResult(Utility.RESULT_CODE_GLOBAL_CAT, intent);
                            finish();
                        }
                    }else{
                        Toast.makeText(SelectCategoryActivity.this, "Please try again. "+jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }
        finally {
            Utility.dismissDialog();
        }

    }
}
