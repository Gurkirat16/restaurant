package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Adapters.ResvRequestsAdapter;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.Fragments.FragmentActiveResv;
import com.reservation.restaurant.Fragments.FragmentResvRequests;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;

public class RsvRequestsActivity extends AppCompatActivity implements MyResponse{

    @BindView(R.id.topTabLayout)
    TabLayout tabLayout;
    @BindView(R.id.topViewPager)
    ViewPager viewPager;
    MyAdapter myAdapter;

    ArrayList<Reservations> listResvReq = new ArrayList<>();
    ArrayList<Reservations> listActiveResv = new ArrayList<>();
    Context context = RsvRequestsActivity.this;
    MyDB myDB;
    int resId;
    int whichRequest;
    Dialog pd;
    private static boolean resvReqVisible = false;
    boolean isFromTablesActivity = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rsv_requests);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Reservations");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        UserSessionManager sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);

        Intent get = getIntent();
        if(get.hasExtra("isFromTablesActivity"))
            isFromTablesActivity = get.getBooleanExtra("isFromTablesActivity", false);

        initMyDB();

        whichRequest = Utility.GET_RESV_REQUEST;
        handler.sendEmptyMessage(whichRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(this);
        myDB.initRequestQueue(this);

    }
    @Override
    protected void onStart() {
        super.onStart();
        resvReqVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        resvReqVisible = false;
    }

    public Fragment getSecondFragmentReference(){
        return myAdapter.getRegisteredFragment(1);
    }
    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK){
                if(whichRequest ==   Utility.GET_RESV_REQUEST || whichRequest == Utility.GET_ACTIVE_RESV){
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            try{
                JSONObject jsonObject;

                switch (msg.what){
                    case Utility.GET_RESV_REQUEST:
                        jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_RESV_REQUESTS, jsonObject);
                        break;
                    case Utility.GET_ACTIVE_RESV:
                        jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_ACTIVE_RESV, jsonObject);
                        break;

                }
            }catch (JSONException e){
                e.printStackTrace();
                Utility.dismissDialog();
            }
        }
    };


    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");
            switch (whichRequest){
                case Utility.GET_RESV_REQUEST:
                    if(resvReqVisible){
                        if(errorCode == 0){
                            if(jsonObject.getInt("num_resv_req") > 0){
                                GsonBuilder builder = new GsonBuilder();
                                builder.serializeNulls();
                                Gson gson = builder.create();
                                listResvReq.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonObject.getJSONArray("reservations")), Reservations[].class)));
                            }
                        }
                        whichRequest = Utility.GET_ACTIVE_RESV;
                        handler.sendEmptyMessage(whichRequest);
                        if(errorCode == 1){
                            //some error in fetching
                        }
                    }
                    break;
                    case Utility.GET_ACTIVE_RESV:
                        if(errorCode == 0){
                            if(jsonObject.getInt("num_active_resv") > 0){
                                GsonBuilder builder = new GsonBuilder();
                                builder.serializeNulls();
                                Gson gson = builder.create();
                                listActiveResv.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonObject.getJSONArray("reservations")), Reservations[].class)));
                            }
                        }
                        if(errorCode == 0 || errorCode == 2){
                            Utility.dismissDialog();
                            myAdapter = new MyAdapter(getSupportFragmentManager());
                            viewPager.setAdapter(myAdapter);
                            tabLayout.setupWithViewPager(viewPager);
                            if(isFromTablesActivity)
                                viewPager.setCurrentItem(1);
                            //no active resv
                        }else{
                            Utility.dismissDialog();
                            //some error in fetching
                        }

                        break;
            }
        }catch (JSONException e){
            Utility.dismissDialog();
            e.printStackTrace();
        }
    }

    public class MyAdapter extends FragmentStatePagerAdapter{
        String[] titles = {"Requests", "Active"};
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public MyAdapter(FragmentManager fm){
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            if(position==0)
                fragment = FragmentResvRequests.newInstance(listResvReq, getApplicationContext());
            else if(position == 1)
                fragment = FragmentActiveResv.newInstance(listActiveResv, getApplicationContext());
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        public Fragment getRegisteredFragment(int position){
            return registeredFragments.get(position);
        }
    }
}
