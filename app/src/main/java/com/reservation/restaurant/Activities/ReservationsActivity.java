package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.gordonwong.materialsheetfab.MaterialSheetFab;
import com.reservation.restaurant.Beans.Reservations;

import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Fragments.CategoriesResvFragment;
import com.reservation.restaurant.Fragments.FragmentEmptyState;
import com.reservation.restaurant.Fragments.FragmentResvTopViewPager;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.Fab;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ReservationsActivity extends AppCompatActivity implements MyResponse, View.OnClickListener, DatePickerDialog.OnDateSetListener{

    ArrayList<Reservations> listResvWalkIn = new ArrayList<>();
    ArrayList<Reservations> listResvChatbot = new ArrayList<>();
    HashMap<String, List<Reservations>> hashMapReservations;


    LinkedHashMap<String, ArrayList<Reservations>> hashMapResvChatbot = new LinkedHashMap<>();

    ArrayList<Reservations> listResvCategory;

    ArrayList<ArrayList<Reservations>> wholeListChatbot;

    @BindView(R.id.grpByDate)
    LinearLayout grpByDate;
    @BindView(R.id.chooseDate)
    LinearLayout chooseDate;
    @BindView(R.id.showAll)
    LinearLayout showAll;
    @BindView(R.id.content_frame)
    FrameLayout contentFrame;
    @BindView(R.id.topViewPager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.fab)
    Fab fab;


    MaterialSheetFab materialSheetFab;
    boolean onDateChosen;
    String chosenDate;
    MyDB myDB;
    int resId;
    int whichRequest;
    Context context = ReservationsActivity.this;
    Dialog pd;
    private static boolean reservationsVisible = false;
    MyPagerAdapter myPagerAdapter;
    int curFragPos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reservations);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("History");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);
        UserSessionManager sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));


        //        setting listeners on fab sheet items
        grpByDate.setOnClickListener(this);
        chooseDate.setOnClickListener(this);
        showAll.setOnClickListener(this);

        View sheetView = findViewById(R.id.fab_sheet);
        View overlay = findViewById(R.id.overlay);
        int sheetColor = getResources().getColor(R.color.background_card);
        int fabColor = getResources().getColor(R.color.colorPrimary);
        // Initialize material sheet FAB
        materialSheetFab = new MaterialSheetFab<>(fab, sheetView, overlay,
                sheetColor, fabColor);

        initMyDB();

        whichRequest = Utility.GET_RESERVATIONS;
        handler.sendEmptyMessage(whichRequest);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(this);
        myDB.initRequestQueue(this);

    }
    @Override
    protected void onStart() {
        super.onStart();
        reservationsVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        reservationsVisible = false;
    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK){
                if(whichRequest ==   Utility.GET_RESERVATIONS){
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        //initMyDB();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.grpByDate:
                materialSheetFab.hideSheet();

                onDateChosen = false;

                if (reservationsVisible){
                    curFragPos = viewPager.getCurrentItem();
                    Fragment curFrag = myPagerAdapter.getRegisteredFragment(curFragPos);
                    ((FragmentResvTopViewPager)curFrag).showDateView(onDateChosen, null);
                }

                break;
            case R.id.chooseDate:
                materialSheetFab.hideSheet();

                Calendar now = Calendar.getInstance();

                DatePickerDialog dateDialog = DatePickerDialog.newInstance(
                        ReservationsActivity.this,
                        now.get(Calendar.YEAR),
                        now.get(Calendar.MONTH),
                        now.get(Calendar.DAY_OF_MONTH)
                );
                dateDialog.setAccentColor(getResources().getColor(R.color.colorPrimary));
                dateDialog.show(getFragmentManager(), "DatePickerDialog");
                break;
            case R.id.showAll:
                materialSheetFab.hideSheet();

                if(reservationsVisible){
                    curFragPos = viewPager.getCurrentItem();
                    Fragment curFrag = myPagerAdapter.getRegisteredFragment(curFragPos);
                    ((FragmentResvTopViewPager)curFrag).showRecentResv();
                }
                break;
        }
    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        onDateChosen = true;
        chosenDate = String.format("%d-%02d-%02d", year, ++monthOfYear, dayOfMonth);
        curFragPos = viewPager.getCurrentItem();
        Fragment curFrag = myPagerAdapter.getRegisteredFragment(curFragPos);
        ((FragmentResvTopViewPager)curFrag).showDateView(onDateChosen, chosenDate);
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case Utility.GET_RESERVATIONS:
                    try{
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_RESERVATIONS, jsonObject);
                    }catch (JSONException e){
                        e.printStackTrace();
                        Utility.dismissDialog();
                    }
                    break;
            }
        }
    };

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");
            switch (whichRequest){
                case Utility.GET_RESERVATIONS:
                    if(reservationsVisible){
                        if(errorCode == 0){
                            if(jsonObject.getInt("num_rows") > 0){
                                GsonBuilder gsonBuilder = new GsonBuilder();
                                gsonBuilder.serializeNulls();
                                Gson gson = gsonBuilder.create();
                                JSONArray jsonArrayChatbotResv = jsonObject.getJSONArray("chatbotResv");
                                JSONArray jsonArrayManualResv = jsonObject.getJSONArray("manualResv");

                                if(jsonArrayChatbotResv.length() > 0){
                                    listResvChatbot.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonArrayChatbotResv), Reservations[].class)));
                                }
                                if(jsonArrayManualResv.length() > 0){
                                    listResvWalkIn.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonArrayManualResv), Reservations[].class)));
                                }

                                JSONArray notResvCategoriesChatbot = jsonObject.getJSONArray("notResvCategoriesChatbot");
                                for(int i=0;i<notResvCategoriesChatbot.length();i++){
                                    Reservations resv = new Reservations();
                                    TableCategoryBean categoryBean = new TableCategoryBean();
                                    categoryBean.setcName((String)notResvCategoriesChatbot.get(i));
                                    resv.setrCategoryInfo(categoryBean);
                                    listResvChatbot.add(resv);
                                }

                                JSONArray notResvCategoriesManual = jsonObject.getJSONArray("notResvCategoriesManual");
                                for(int i=0;i<notResvCategoriesManual.length();i++){
                                    Reservations resv = new Reservations();
                                    TableCategoryBean categoryBean = new TableCategoryBean();
                                    categoryBean.setcName((String)notResvCategoriesManual.get(i));
                                    resv.setrCategoryInfo(categoryBean);
                                    listResvWalkIn.add(resv);
                                }

                              // JSONArray jsonArray = jsonObject.getJSONArray("reservations");

                                /*for(int i=0;i<jsonArray.length();i++) {
                                    JSONObject rsvJSONObj = (JSONObject) jsonArray.get(i);
                                    JSONObject rsvCusInfo = rsvJSONObj.getJSONObject("rCusInfo");
                                    CustomerInfo cusObj = new CustomerInfo(rsvCusInfo.getInt("cusId"),
                                            rsvCusInfo.getString("cusName"), rsvCusInfo.getString("cusEmail"),
                                            rsvCusInfo.getString("cusPhone"), rsvCusInfo.getInt("numMembers"),
                                            rsvCusInfo.getInt("rCmbId"));

                                    JSONObject rsvCategoryInfo = rsvJSONObj.getJSONObject("rCategoryInfo");
                                    TableCategoryBean catObj = new TableCategoryBean(rsvCategoryInfo.getInt("cId"),
                                            rsvCategoryInfo.getString("cName"), rsvCategoryInfo.getInt("resId"),
                                            rsvCategoryInfo.getString("cImg"), rsvCategoryInfo.getString("cDesc")
                                            );
                                    Log.i("show", "done till here");

                                    ArrayList<Table> listTables = new ArrayList<>();
                                    JSONArray arrTableInfo = rsvJSONObj.getJSONArray("arrTableInfo");
                                    for(int j=0;j<arrTableInfo.length();j++){
                                        JSONObject obj = arrTableInfo.getJSONObject(j);
                                        Table table = new Table(obj.getInt("tId"), obj.getInt("cId"),
                                                obj.getString("tableName"), obj.getInt("numChairs"),
                                                obj.getInt("statusMergeable"), obj.getInt("statusActive"));
                                        listTables.add(table);
                                    }

                                   *//* GsonBuilder gsonBuilder = new GsonBuilder();
                                    gsonBuilder.serializeNulls();
                                    Gson gson = gsonBuilder.create();
                                    listTables.addAll(Arrays.asList(gson.fromJson(String.valueOf(rsvJSONObj.getJSONArray("arrTableInfo")), Table[].class)));
*//*
                                    Reservations rsv = new Reservations(rsvJSONObj.getString("rDate"), rsvJSONObj.getString("rStartTime"),
                                            rsvJSONObj.getString("rEndTime"), rsvJSONObj.getInt("rStatus"), rsvJSONObj.getInt("rCmbId"),
                                            rsvJSONObj.getInt("resId"), cusObj, catObj, listTables);

                                    listReservations.add(rsv);
                                }*/
                            }
                        }/*else if(errorCode == 2 || errorCode == 3){
                            fab.setVisibility(View.GONE);
                            contentFrame.setVisibility(View.VISIBLE);
                            getSupportFragmentManager().beginTransaction()
                                    .replace(R.id.content_frame, FragmentEmptyState.newInstance(R.layout.empty_reservations))
                                    .commit();
                        }*/

                        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
                        viewPager.setAdapter(myPagerAdapter);
                        viewPager.setCurrentItem(0);
                        tabLayout.setupWithViewPager(viewPager);

                        Utility.dismissDialog();
                    }
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }finally {
            Utility.dismissDialog();
        }
    }

    public class MyPagerAdapter extends FragmentStatePagerAdapter{

        String[] titles = {"Chatbot", "WalkIns"};
        SparseArray<Fragment> registeredFragments = new SparseArray<>();

        public MyPagerAdapter(FragmentManager fm){
            super(fm);
        }
        @Override
        public Fragment getItem(int position) {
            Fragment fragment;
            if(position == 0) {
                fragment = FragmentResvTopViewPager.newInstance(listResvChatbot);
            }
            else {
                fragment = FragmentResvTopViewPager.newInstance(listResvWalkIn);
            }
            registeredFragments.put(position, fragment);
            return fragment;
        }

        @Override
        public int getCount() {
            return 2;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return titles[position];
        }

        public Fragment getRegisteredFragment(int position){
            return registeredFragments.get(position);
        }
    }

}
