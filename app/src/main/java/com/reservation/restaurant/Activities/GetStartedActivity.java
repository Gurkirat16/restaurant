package com.reservation.restaurant.Activities;

import android.app.ActionBar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.ToxicBakery.viewpager.transforms.CubeInTransformer;
import com.ToxicBakery.viewpager.transforms.CubeOutTransformer;
import com.ToxicBakery.viewpager.transforms.RotateUpTransformer;
import com.reservation.restaurant.Fragments.FragmentCategory;
import com.reservation.restaurant.Fragments.FragmentMerge;
import com.reservation.restaurant.Fragments.FragmentNotif;
import com.reservation.restaurant.Fragments.FragmentTables;
import com.reservation.restaurant.R;


import butterknife.BindView;
import butterknife.ButterKnife;

public class GetStartedActivity extends AppCompatActivity {

    static FragmentCategory fragmentCategory;
    static FragmentTables fragmentTables;
    static FragmentMerge fragmentMerge;
    static FragmentNotif fragmentNotif;

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    MyPagerAdapter myPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_get_started);

        ButterKnife.bind(this);

        fragmentCategory = new FragmentCategory();
        fragmentTables = new FragmentTables();
        fragmentMerge = new FragmentMerge();
        fragmentNotif = new FragmentNotif();

        myPagerAdapter = new MyPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(myPagerAdapter);
        viewPager.setPageTransformer(true, new CubeOutTransformer());
    }


    public static class MyPagerAdapter extends FragmentPagerAdapter{

        public MyPagerAdapter(FragmentManager fragmentManager) {
            super(fragmentManager);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return fragmentCategory;
                case 1:
                    return fragmentTables;
                case 2:
                    return fragmentMerge;
                case 3:
                    return fragmentNotif;
                default:
                    return null;
            }
        }

        @Override
        public int getCount() {
            return 4;
        }
    }
}