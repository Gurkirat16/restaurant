package com.reservation.restaurant.Activities;


import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Fragments.FragmentWalkIns;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import com.wdullaer.materialdatetimepicker.date.DatePickerDialog;
import com.wdullaer.materialdatetimepicker.time.TimePickerDialog;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalkInsActivity extends AppCompatActivity implements MyResponse, DatePickerDialog.OnDateSetListener, TimePickerDialog.OnTimeSetListener, View.OnClickListener{

    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.chooseCategoryWalkIn)
    Spinner spChooseCategory;

    @BindView(R.id.numMembers)
    EditText etxtnumMembers;

    @BindView(R.id.rgpWalkIns)
    RadioGroup rgpTables;

    MyDB myDB;
    int resId, cId, whichReq;
    UserSessionManager sessionManager;
    ArrayList<TableCategoryBean> categoryObjectList;
    ArrayList<String> categoryNameList, availableTablesName = new ArrayList<>();
    ArrayAdapter<String> adapterCategories;
    ArrayList<Table> availableTables = new ArrayList<>();
    boolean noCategories = false;
    Context context = WalkInsActivity.this;
    Dialog pd;
    Button btnChooseDate, btnChooseTimeFrom, btnChooseTimeTo, btnCheckAvail;
    Calendar calendar;
    TextView txtDate,txtTimeTo, txtTimeFrom;
    String currentDate, currentTime, date, timeTo, timeFrom, category, numMembers;
    Boolean isArrivalTime;

    FragmentWalkIns dialogFrag;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reserve_table);

        ButterKnife.bind(this);

        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);

        categoryObjectList = new ArrayList<>();
        categoryNameList = new ArrayList<>();

        sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        initDB();
        fetchCategories();
    }

    void initActivity(){
        txtDate=(TextView)findViewById(R.id.txtDate);
        txtTimeTo=(TextView)findViewById(R.id.txtTimeTo);
        txtTimeFrom=(TextView)findViewById(R.id.txtTimeFrom);
        btnChooseDate=(Button)findViewById(R.id.btnChooseDate);
        btnChooseTimeFrom=(Button)findViewById(R.id.btnChooseTimeFrom);
        btnChooseTimeTo=(Button)findViewById(R.id.btnChooseTimeTo);
        btnCheckAvail = (Button)findViewById(R.id.btnCheckAvail);

        btnChooseDate.setOnClickListener(this);
        btnChooseTimeTo.setOnClickListener(this);
        btnChooseTimeFrom.setOnClickListener(this);
        btnCheckAvail.setOnClickListener(this);

        SimpleDateFormat sdfDate = new SimpleDateFormat("yyyy-MM-dd");
        currentDate = sdfDate.format(new Date());
        SimpleDateFormat sdfTime = new SimpleDateFormat("H:mm");
        currentTime = sdfTime.format(new Date());

//        currentDate = DateFormat.getDateTimeInstance().format(new Date());
        txtDate.setText(currentDate);
//
//        currentTime = String.valueOf(Calendar.getInstance().getTime());
        List<String> timeArr = Arrays.asList(currentTime.split(":"));
        if(Integer.parseInt(timeArr.get(0)) >= 10 && Integer.parseInt(timeArr.get(0)) <= 22){
            txtTimeFrom.setText(currentTime);
            txtTimeTo.setText(currentTime);
        } else {
            txtTimeFrom.setText("10:00");
            txtTimeTo.setText("10:00");
        }

        adapterCategories = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        adapterCategories.add("--Select Category--");
        adapterCategories.addAll(categoryNameList);
        spChooseCategory.setAdapter(adapterCategories);
        spChooseCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                cId = categoryObjectList.get(i+1).getcId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK) {
                if (whichReq == Utility.GET_CATEGORIES_LIST) {
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    void fetchCategories(){
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("resId", resId);
            whichReq = Utility.GET_CATEGORIES_LIST;
            myDB.processRequest(Request.Method.POST, Utility.URL_GET_CATEGORIES, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    boolean validate(){
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        try {
            Date date1 = formatter.parse(currentDate);
            Date date2 = formatter.parse(date);

            if (date1.compareTo(date2) > 0){
                txtDate.setError("Please enter valid date");
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        SimpleDateFormat formatter1 = new SimpleDateFormat("H:mm");
        try {
            Date time1 = formatter1.parse(timeFrom);
            Date time2 = formatter1.parse(timeTo);

            if (time1.compareTo(time2) > 0 || time1.compareTo(time2) == 0){
                txtTimeFrom.setError("Please enter valid time");
                txtTimeTo.setError("Please enter valid time");
                return false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if(category.equals("--Select Category--")){
            TextView selected = (TextView) spChooseCategory.getSelectedView();
            String err = selected.getResources().getString(R.string.err);
            selected.setError(err);
            Toast.makeText(WalkInsActivity.this, "Please select a category", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(numMembers.isEmpty()){
            etxtnumMembers.setError("Please fill number of members!");
        }

        return true;
    }

    void getAvailableTables(){
        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("cId", cId);
            jsonObject.put("numMem", numMembers);
            jsonObject.put("date", date);
            jsonObject.put("startTime", timeFrom);
            jsonObject.put("endTime", timeTo);
            whichReq = Utility.GET_AVAILABLE_TABLES;
            myDB.processRequest(Request.Method.POST, Utility.URL_GET_AVAILABLE_TABLES, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMyResponse(JSONObject jsonObject) {

        try {
            int error = jsonObject.getInt("error");
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            Gson gson = gsonBuilder.create();

            switch (whichReq){
                case Utility.GET_CATEGORIES_LIST:
                    if(error == 0){

                        JSONArray jsonArray = jsonObject.getJSONArray("categories");
                        if(jsonArray != null) {
                            categoryObjectList.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonArray), TableCategoryBean[].class)));
                            for (int i = 0; i < categoryObjectList.size(); i++) {
                                categoryNameList.add(categoryObjectList.get(i).getcName());
                            }
                            Log.i("show", "inFloorMap categoryList: "+categoryObjectList.toString());

                        } else {
                            Toast.makeText(WalkInsActivity.this, "Kindly create categories first", Toast.LENGTH_SHORT).show();
                        }

                    } else if(error == 2){

                        noCategories = true;
                        LayoutInflater inflater = LayoutInflater.from(context);
                        View inflatedView = inflater.inflate(R.layout.empty_categories, null, false);
                        container.addView(inflatedView);
                    }
                    Utility.dismissDialog();
                    initActivity();
                    break;

                case Utility.GET_AVAILABLE_TABLES:

                    if(error == 0){
                        JSONArray array = jsonObject.getJSONArray("tableInfoArr");
                        availableTables.addAll(Arrays.asList(gson.fromJson(String.valueOf(array), Table[].class)));
                        for (Table table:availableTables) {
                            availableTablesName.add(table.getTableName());
                        }

                        Bundle bundle = new Bundle();
                        bundle.putStringArrayList("availableTables", availableTablesName);
                        Log.i("show", "in act: "+availableTablesName.toString());
                        BottomSheetDialogFragment bottomSheetDialogFragment = new FragmentWalkIns();
                        bottomSheetDialogFragment.setArguments(bundle);
                        bottomSheetDialogFragment.show(getSupportFragmentManager(), bottomSheetDialogFragment.getTag());

                    } else {
                        Toast.makeText(context, "Sorry! No Table Available", Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissDialog();

                    break;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void onDateSet(DatePickerDialog view, int year, int monthOfYear, int dayOfMonth) {
        monthOfYear += 1;
        txtDate.setText(year+"-"+monthOfYear+"-"+dayOfMonth);
    }

    @Override
    public void onTimeSet(TimePickerDialog view, int hourOfDay, int minute, int second) {
        String seperator = ":";
        if (String.valueOf(minute).length() == 1){
            seperator = ":0";
        }
        if (isArrivalTime)
            txtTimeFrom.setText(hourOfDay+seperator+minute);
        else
            txtTimeTo.setText(hourOfDay+seperator+minute);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btnChooseTimeTo:
                isArrivalTime = false;
                String arrivalTime = txtTimeFrom.getText().toString().trim();
                List<String> timeArr = Arrays.asList(arrivalTime.split(":"));
                int hr = Integer.parseInt(timeArr.get(0));
                int min = Integer.parseInt(timeArr.get(1));
                Calendar now = Calendar.getInstance();
                TimePickerDialog tpdTo = TimePickerDialog.newInstance(
                        WalkInsActivity.this,
                        now.get(Calendar.HOUR_OF_DAY),
                        now.get(Calendar.MINUTE),
                        true
                );

                if (hr >= 10 && hr <= 22)
                    tpdTo.setMinTime(hr,min,0);
                else
                    tpdTo.setMinTime(10,0,0);
                tpdTo.setMaxTime(22,0,0);
                tpdTo.setVersion(TimePickerDialog.Version.VERSION_1);
                tpdTo.setAccentColor("#FF5722");
                tpdTo.dismissOnPause(true);
                tpdTo.show(getFragmentManager(), "Timepickerdialog");

                break;

            case R.id.btnChooseTimeFrom:
                isArrivalTime = true;
                Calendar c = Calendar.getInstance();
                TimePickerDialog tpdFrom = TimePickerDialog.newInstance(
                        WalkInsActivity.this,
                        c.get(Calendar.HOUR_OF_DAY),
                        c.get(Calendar.MINUTE),
                        true
                );
                tpdFrom.setVersion(TimePickerDialog.Version.VERSION_1);
                tpdFrom.setMinTime(10, 0, 0);
                //tpdFrom.setMinTime();
                tpdFrom.setMaxTime(19,0,0);
                tpdFrom.setAccentColor("#FF5722");
                tpdFrom.dismissOnPause(true);
                tpdFrom.show(getFragmentManager(), "Timepickerdialog");

                break;

            case R.id.btnChooseDate:
                Calendar calendar = Calendar.getInstance();
                DatePickerDialog dpd = DatePickerDialog.newInstance(
                        WalkInsActivity.this,
                        calendar.get(Calendar.YEAR),
                        calendar.get(Calendar.MONTH),
                        calendar.get(Calendar.DAY_OF_MONTH)
                );
                dpd.setVersion(DatePickerDialog.Version.VERSION_1);
                dpd.setAccentColor("#FF5722");
                dpd.dismissOnPause(true);
//                dpd.setMaxDate();
                dpd.show(getFragmentManager(), "DatePickerDialog");

                break;

            case R.id.btnCheckAvail:
                date = txtDate.getText().toString().trim();
                timeTo = txtTimeTo.getText().toString().trim();
                timeFrom = txtTimeFrom.getText().toString().trim();
                category = (String)spChooseCategory.getSelectedItem();
                numMembers = etxtnumMembers.getText().toString().trim();

                if (validate()){

                    cId = categoryObjectList.get(spChooseCategory.getSelectedItemPosition() - 1).getcId();
                    getAvailableTables();
                    //Toast.makeText(this, "validated", Toast.LENGTH_SHORT).show();
                }

                break;

        }
    }

}
