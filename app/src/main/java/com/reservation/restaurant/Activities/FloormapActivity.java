package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonArray;
import com.reservation.restaurant.Adapters.TablesAdapter;
import com.reservation.restaurant.Beans.Table;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Beans.TableLocationBean;
import com.reservation.restaurant.Fragments.FragmentFloormap;

import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import com.yalantis.contextmenu.lib.ContextMenuDialogFragment;
import com.yalantis.contextmenu.lib.MenuObject;
import com.yalantis.contextmenu.lib.MenuParams;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemClickListener;
import com.yalantis.contextmenu.lib.interfaces.OnMenuItemLongClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.R.attr.fragment;
import static android.R.attr.numberPickerStyle;


public class FloormapActivity extends AppCompatActivity implements OnMenuItemClickListener, OnMenuItemLongClickListener, MyResponse{

    @BindView(R.id.container)
    FrameLayout container;

    private FragmentManager fragmentManager;
    private ContextMenuDialogFragment mMenuDialogFragment;
    MyDB myDB;
    int resId, cId, tId;
    UserSessionManager sessionManager;
    int whichReq;
    ArrayList<TableCategoryBean> categoryObjectList;
    ArrayList<String> categoryNameList;
    ArrayList<Table> tableObjectList, tablesOnScreen, tablesOffScreen, mergeableTables;
    ArrayList<TableLocationBean> tableLocationList;
    FragmentFloormap fragmentFloormap = new FragmentFloormap();
    FragmentTransaction fragmentTransaction;
    Context context = FloormapActivity.this;
    Dialog pd;
    AlertDialog categoriesDialog = null;
    private static boolean floormapVisible = false;
    boolean noCategories = false, noTables = false;

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        floormapVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        floormapVisible = false;
    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK) {
                if (whichReq == Utility.GET_CATEGORIES_LIST || whichReq == Utility.GET_TABLES) {
                    Utility.dismissDialog();
                    finish();
                }
                Log.i("show", "first if");
            }
            return false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_floormap);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Floormap");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initDB();

        ButterKnife.bind(this);
        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);

        sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));
        categoryObjectList = new ArrayList<>();
        tableObjectList = new ArrayList<>();
        categoryNameList = new ArrayList<>();
        tableLocationList = new ArrayList<>();
        tablesOnScreen = new ArrayList<>();
        tablesOffScreen = new ArrayList<>();
        mergeableTables = new ArrayList<>();
        fetchCategories();

    }

    private void initMenuFragment() {
        MenuParams menuParams = new MenuParams();
        menuParams.setActionBarSize(((int) getResources().getDimension(R.dimen.toolbar_height)));
        menuParams.setMenuObjects(getMenuObjects());
        //menu can be closed on touching non-button area
        menuParams.setClosableOutside(true);
        menuParams.setAnimationDuration(100);
//        menuParams.setFitsSystemWindow(true);
//        menuParams.setClipToPadding(false);
        mMenuDialogFragment = ContextMenuDialogFragment.newInstance(menuParams);
        mMenuDialogFragment.setItemClickListener(this);
        mMenuDialogFragment.setItemLongClickListener(this);
    }

    private List<MenuObject> getMenuObjects() {
        // You can use any [resource, bitmap, drawable, color] as image:
        // item.setResource(...)
        // item.setBitmap(...)
        // item.setDrawable(...)
        // item.setColor(...)
        // You can set image ScaleType:
        // item.setScaleType(ScaleType.FIT_XY)
        // You can use any [resource, drawable, color] as background:
        // item.setBgResource(...)
        // item.setBgDrawable(...)
        // item.setBgColor(...)
        // You can use any [color] as text color:
        // item.setTextColor(...)
        // You can set any [color] as divider color:
        // item.setDividerColor(...)

        List<MenuObject> menuObjects = new ArrayList<>();

        MenuObject close = new MenuObject();

        Drawable drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_cancel_white_36dp);
        drawable.setColorFilter(Color.parseColor("#FF5722"), PorterDuff.Mode.SRC_ATOP);
        close.setDrawable(drawable);

        MenuObject merge = new MenuObject("Merge tables");
        drawable = ContextCompat.getDrawable(getApplicationContext(), R.drawable.ic_call_merge_black_36dp);
        drawable.setColorFilter(Color.parseColor("#FF5722"), PorterDuff.Mode.SRC_ATOP);
        merge.setDrawable(drawable);

        drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_view_list_black_36dp);
        drawable.setColorFilter(Color.parseColor("#FF5722"), PorterDuff.Mode.SRC_ATOP);
        MenuObject viewComb = new MenuObject("View combinations");
        viewComb.setDrawable(drawable);

        drawable = ContextCompat.getDrawable(getApplicationContext(),R.drawable.ic_add_black_36dp);
        drawable.setColorFilter(Color.parseColor("#FF5722"), PorterDuff.Mode.SRC_ATOP);
        MenuObject table = new MenuObject("Add Table");
        table.setDrawable(drawable);

        menuObjects.add(close);
        menuObjects.add(merge);
        menuObjects.add(viewComb);
        menuObjects.add(table);

        return menuObjects;
    }

    protected void addFragment(Fragment fragment, boolean addToBackStack, int containerId) {
        invalidateOptionsMenu();
        String backStackName = fragment.getClass().getName();
        boolean fragmentPopped = fragmentManager.popBackStackImmediate(backStackName, 0);
        if (!fragmentPopped) {
            FragmentTransaction transaction = fragmentManager.beginTransaction();
            transaction.add(containerId, fragment, backStackName)
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            if (addToBackStack)
                transaction.addToBackStack(backStackName);
            transaction.commit();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(final Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.floormap_menu, menu);

        MenuItem menuItem = menu.findItem(R.id.floormap_func);
        if(noCategories|| noTables)
            menuItem.setVisible(false);
        else
            menuItem.setVisible(true);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.floormap_func:
                if (fragmentManager.findFragmentByTag(ContextMenuDialogFragment.TAG) == null) {
                    mMenuDialogFragment.show(fragmentManager, ContextMenuDialogFragment.TAG);
                }
                break;
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onBackPressed() {
        if (mMenuDialogFragment != null && mMenuDialogFragment.isAdded()) {
            mMenuDialogFragment.dismiss();
        } else
            finish();
    }

    @Override
    public void onMenuItemClick(View clickedView, int position) {
        switch(position){
            case 0:
                break;
            case 1:
                fragmentFloormap.mergeTables();
                break;
            case 2:
                Intent intent = new Intent(FloormapActivity.this, MergeCmbActivity.class);
                intent.putExtra("cId", cId);
                startActivity(intent);
                break;
            case 3:
                fragmentFloormap.viewTablesOffScreen();
                break;

        }

    }

    @Override
    public void onMenuItemLongClick(View clickedView, int position) {

    }

    void fetchCategories(){

        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("resId", resId);
            whichReq = Utility.GET_CATEGORIES_LIST;
            myDB.processRequest(Request.Method.POST, Utility.URL_GET_CATEGORIES, jsonObject);
        } catch (JSONException e) {
            if(floormapVisible)
                e.printStackTrace();
        }


    }

    @Override
    public void onMyResponse(JSONObject jsonObject) {

        try {
            int error = jsonObject.getInt("error");
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.serializeNulls();
            Gson gson = gsonBuilder.create();

            switch (whichReq){

                case Utility.GET_CATEGORIES_LIST:
                    if(floormapVisible){
                        if(error == 0){

                            JSONArray jsonArray = jsonObject.getJSONArray("categories");
                            if(jsonArray != null) {
                                categoryObjectList.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonArray), TableCategoryBean[].class)));
                                for (int i = 0; i < categoryObjectList.size(); i++) {
                                    categoryNameList.add(categoryObjectList.get(i).getcName());
                                }
                                Log.i("show", "inFloorMap categoryList: "+categoryObjectList.toString());
                                showCategoriesDialog();

                            } else {
                                if(floormapVisible)
                                    Toast.makeText(FloormapActivity.this, "Kindly create categories first", Toast.LENGTH_SHORT).show();
                            }

                        } else if(error == 2){
                            if(floormapVisible){

                                //show dialog
                              /*  AlertDialog.Builder builder;
                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                    builder = new AlertDialog.Builder(context, android.R.style.Theme_Material_Dialog_Alert);
                                } else {
                                    builder = new AlertDialog.Builder(context);
                                }
                                builder.setTitle("No Categories")
                                        .setMessage("It seems you have not created any category yet.")
                                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                finish();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_alert)
                                        .show();*/
                                noCategories = true;
                              invalidateOptionsMenu();
                            LayoutInflater inflater = LayoutInflater.from(context);
                            View inflatedView = inflater.inflate(R.layout.empty_categories_2, null, false);
                            container.addView(inflatedView);

                            }

                        }
                        Utility.dismissDialog();
                    }
                    break;

                case Utility.GET_TABLES:
                    if(floormapVisible){
                        if(error == 0){
                            if(jsonObject.getInt("num_tables") > 0) {

                                JSONArray jsonTablesArr = jsonObject.getJSONArray("tables");
                                tableObjectList.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonTablesArr), Table[].class)));
                                Log.i("show", "tableObjectList: "+tableObjectList.toString());

                                if(jsonObject.getInt("num_location_tables") > 0){
                                    JSONArray jsonLocationArr = jsonObject.getJSONArray("tablesLocation");
                                    tableLocationList.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonLocationArr), TableLocationBean[].class)));
                                    for(TableLocationBean l : tableLocationList){
                                        int index = 0;
                                        for(Table t : tableObjectList){
                                            if(l.gettId() == t.gettId()){
                                                tablesOnScreen.add(tableObjectList.get(index));
                                            }
                                            index++ ;
                                        }
                                    }

                                    Log.i("show", "tablesOffScreen: "+tablesOffScreen.toString());
                                } else {
                                    if(floormapVisible)
                                        Toast.makeText(FloormapActivity.this, "Location not set for any table", Toast.LENGTH_SHORT).show();
                                }
                                for(Table t : tableObjectList){
                                    if(!tablesOnScreen.contains(t))
                                        tablesOffScreen.add(t);
                                }
                                for(Table t : tableObjectList){
                                    if(t.getStatusMergeable() == 1){
                                        mergeableTables.add(t);
                                    }
                                }
                                handler.sendEmptyMessage(Utility.MOVE_TO_FRAGMENT);
                            }
                        } else if(error == 2){
                            if(floormapVisible){
                                //show dialog
                                noTables = true;
                                invalidateOptionsMenu();
                                LayoutInflater inflater = LayoutInflater.from(context);
                                View inflatedView = inflater.inflate(R.layout.empty_tables, null, false);
                                container.addView(inflatedView);
                            }
                        }
                        Utility.dismissDialog();
                    }
                    break;

            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.dismissDialog();
        }

    }

    void showCategoriesDialog(){
        AlertDialog.Builder builderSingle = new AlertDialog.Builder(this);
        builderSingle.setTitle("Select Category to view tables");
        builderSingle.setCancelable(false);
        final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(this, R.layout.simple_list_item_alert_dia);
        arrayAdapter.addAll(categoryNameList);

        builderSingle.setAdapter(arrayAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cId = categoryObjectList.get(which).getcId();
                fetchTables();
            }
        });
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        if(floormapVisible) {
            categoriesDialog = builderSingle.create();
            categoriesDialog.setOnKeyListener(keyListener);
            builderSingle.show();
        }
    }

    void fetchTables(){
        Utility.initDialog(context);
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("resId", resId);
            jsonObject.put("cId",cId);
            whichReq = Utility.GET_TABLES;
            myDB.processRequest(Request.Method.POST, Utility.URL_GET_TABLES_WITH_LOCATION, jsonObject);
        } catch (JSONException e) {
            e.printStackTrace();
            Utility.dismissDialog();
        }

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == Utility.MOVE_TO_FRAGMENT) {
                Log.i("show", "msg = 100, create frag");
                fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                initMenuFragment();
                Bundle bundle = new Bundle();
                bundle.putInt("cId", cId);
                bundle.putParcelableArrayList("tablesOnScreen", tablesOnScreen);
                bundle.putParcelableArrayList("tablesOffScreen", tablesOffScreen);
                bundle.putParcelableArrayList("tableLocationList", tableLocationList);
                bundle.putParcelableArrayList("mergeableTables", mergeableTables);
                fragmentFloormap.setArguments(bundle);
                if(floormapVisible){
                    fragmentTransaction.add(R.id.container, fragmentFloormap);
                    fragmentTransaction.commit();
                }
            }
        }
    };


}