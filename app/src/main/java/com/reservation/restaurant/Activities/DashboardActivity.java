package com.reservation.restaurant.Activities;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;

public class DashboardActivity extends AppCompatActivity implements View.OnClickListener, MyResponse {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.createCategory)
    LinearLayout lcreateCategory;

    @BindView(R.id.enterTables)
    LinearLayout lenterTables;

    @BindView(R.id.mergeTables)
    LinearLayout lmergeTables;

    @BindView(R.id.reservations)
    LinearLayout lnotifications;

    @BindView(R.id.resReq)
    LinearLayout lresvRequests;

    @BindView(R.id.reserveTables)
    LinearLayout lreserveTables;

    int whichRequest;
    MyDB myDB;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    Context context = DashboardActivity.this;
    int resId;
    UserSessionManager sessionManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        setSupportActionBar(mToolbar);
        ButterKnife.bind(this);


        sessionManager = new UserSessionManager(this);
        sharedPreferences =  getSharedPreferences(Utility.SHARED_PREF_NAME, 0);
        editor = sharedPreferences.edit();
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        initDB();

        lcreateCategory.setOnClickListener(this);
        lenterTables.setOnClickListener(this);
        lmergeTables.setOnClickListener(this);
        lnotifications.setOnClickListener(this);
        lresvRequests.setOnClickListener(this);
        lreserveTables.setOnClickListener(this);

        if(sharedPreferences.getBoolean(Utility.SHP_SHOW_SELECT_CAT, true)){
            Utility.initDialog(context);
            whichRequest = Utility.GET_CATEGORY_COUNT;
            handler.sendEmptyMessage(Utility.GET_CATEGORY_COUNT);
        }
    }

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch(id){

            case R.id.createCategory:
                Intent i;
               /* if(sharedPreferences.getBoolean(Utility.SHP_SHOW_SELECT_CAT, true))
                    i = new Intent(DashboardActivity.this, SelectCategoryActivity.class);
                else*/
                i = new Intent(DashboardActivity.this, CreateCategoryActivity.class);

                startActivity(i);
                break;

            case R.id.enterTables:
                Intent j = new Intent(DashboardActivity.this, EnterTablesActivity.class);
                startActivity(j);
                break;

            case R.id.mergeTables:
                Intent k = new Intent(DashboardActivity.this, FloormapActivity.class);
                startActivity(k);
                break;
            case R.id.reservations:
                Intent l = new Intent(DashboardActivity.this, ReservationsActivity.class);
                startActivity(l);
                break;
            case R.id.resReq:
                Intent m = new Intent(DashboardActivity.this, RsvRequestsActivity.class);
                startActivity(m);
                break;

            case R.id.reserveTables:
                Intent n = new Intent(DashboardActivity.this, WalkInsActivity.class);
                startActivity(n);
                break;

        }

    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case Utility.GET_CATEGORY_COUNT:
                    try{
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_CATEGORY_COUNT, jsonObject);
                    }catch (JSONException e){
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        switch (whichRequest){
            case Utility.GET_CATEGORY_COUNT:
                try{
                    if(jsonObject.getInt("error") == 0){
                        if(jsonObject.getInt("num_categories") > 0) {
                            editor.putBoolean(Utility.SHP_SHOW_SELECT_CAT, false);
                            editor.commit();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissDialog();
                }catch(JSONException e){
                    e.printStackTrace();
                    Utility.dismissDialog();
                }
                break;
        }
    }
}