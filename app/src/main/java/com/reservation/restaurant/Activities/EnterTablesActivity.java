package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.Fab;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import com.suke.widget.SwitchButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class EnterTablesActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener, MyResponse{
    @BindView(R.id.llCreateTable)
    LinearLayout llCreateTable;

    @BindView(R.id.tableName)
    EditText tableName;

    @BindView(R.id.numChairs)
    EditText numChairs;

    @BindView(R.id.switch_btn_mergeable)
    SwitchButton sbMergeable;

    @BindView(R.id.switch_btn_status)
    SwitchButton sbStatus;

    @BindView(R.id.chooseCategory)
    Spinner spChooseCategory;

    @BindView(R.id.container)
    FrameLayout container;

    @BindView(R.id.btn_create_table)
    Button btnCreateTable;

    ArrayAdapter<String> spinnerAdapter;
    ArrayAdapter<String> adapterCategories;
    ArrayList<String> categories = new ArrayList<>();
    ArrayList<TableCategoryBean> categoryObject = new ArrayList<>();

    MyDB myDB;

    JSONArray array;

    int resId;
    Dialog pd;
    Context context = EnterTablesActivity.this;

    @BindView(R.id.fabViewTable)
    FloatingActionButton fab;

    String strTableName, strNumChairs, choosenCategory;
    int statusActive, isMergeable;
    int whichReq,cId;
    UserSessionManager sessionManager;
    private boolean enterTablesVisible = false;

    void initDB(){
        myDB = MyDB.getDBObject();
        myDB.registerMyListener(this);
        myDB.initRequestQueue(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        enterTablesVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        enterTablesVisible = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);

        initDB();
        try {
            if(!categoryObject.isEmpty()){
                categoryObject.clear();
                adapterCategories.notifyDataSetChanged();
            }
            if(!categories.isEmpty()){
                categories.clear();
                spinnerAdapter.notifyDataSetChanged();
            }

            JSONObject jsonObject = new JSONObject();
            jsonObject.put("resId",resId);
            whichReq = Utility.GET_CATEGORIES_ENTER_TABLES;
            if(enterTablesVisible){
                myDB.processRequest(Request.Method.POST, Utility.URL_GET_CATEGORIES, jsonObject);
            }

        } catch (JSONException e) {
            e.printStackTrace();
            Utility.dismissDialog();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i("show", "onCreate");
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_tables);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Tables");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        sessionManager = new UserSessionManager(this);
        resId = Integer.parseInt(sessionManager.getUserDetails().get(Utility.SHPRESID));

        spinnerAdapter = new ArrayAdapter<String>(EnterTablesActivity.this, R.layout.simple_list_item_alert_dia);

        adapterCategories = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_dropdown_item);
        adapterCategories.add("--Select Table Categories--");

        spChooseCategory.setAdapter(adapterCategories);
        spChooseCategory.setOnItemSelectedListener(this);

        btnCreateTable.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addTable();
            }
        });

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                chooseCategory();
            }

        });
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Log.i("show", "back");
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK){
                if(whichReq == Utility.GET_CATEGORIES_ENTER_TABLES){
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    boolean validate(){
        if(choosenCategory.equals("--Select Table Categories--")){
            TextView selected = (TextView) spChooseCategory.getSelectedView();
            String err = selected.getResources().getString(R.string.err);
            selected.setError(err);
            Toast.makeText(EnterTablesActivity.this, "Please select a category for this table !", Toast.LENGTH_SHORT).show();
            return false;
        }

        if(strTableName.isEmpty()){
            tableName.setError("Field can't be empty");
            return false;
        }

        if(strNumChairs.isEmpty()){
            numChairs.setError("Field can't be empty");
            return false;
        }else if(Integer.parseInt(strNumChairs) == 0){
            numChairs.setError("Chairs can't be 0");
            return false;
        }

        return true;

    }

    void clearFields(){
        tableName.getText().clear();
        numChairs.getText().clear();
        sbMergeable.setChecked(false);
        sbStatus.setChecked(false);
        spChooseCategory.setSelection(0);
    }

    void addTable(){

            choosenCategory = (String) spChooseCategory.getSelectedItem();
            strTableName = tableName.getText().toString().trim();
            strNumChairs = numChairs.getText().toString().trim();
            statusActive = sbStatus.isChecked() ? 1 : 0;
            isMergeable = sbMergeable.isChecked() ? 1 : 0;

            if (validate()) {
                pd = Utility.initDialog(context);

                cId = categoryObject.get(spChooseCategory.getSelectedItemPosition() - 1).getcId();
                try {
                    JSONObject obj = new JSONObject();
                    obj.put("resId", resId);
                    obj.put("cId", cId);
                    obj.put("tableName", strTableName);
                    obj.put("numChairs", strNumChairs);
                    obj.put("statusMergeable", isMergeable);
                    obj.put("statusActive", statusActive);
                    whichReq = Utility.ADD_TABLE;
                    myDB.processRequest(Request.Method.POST, Utility.URL_ADD_TABLE, obj);
                } catch (JSONException e) {
                    e.printStackTrace();
                    Utility.dismissDialog();
                }
            }

    }

    void chooseCategory(){

        AlertDialog.Builder builderSingle = new AlertDialog.Builder(EnterTablesActivity.this);
        builderSingle.setTitle("Select Category to view tables");
        spinnerAdapter.addAll(categories);

        builderSingle.setAdapter(spinnerAdapter, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                cId = categoryObject.get(which).getcId();
                Intent i = new Intent(EnterTablesActivity.this, ListTablesActivity.class);
                i.putExtra("cId", cId);
                startActivity(i);
            }
        });
        builderSingle.setNegativeButton("cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        builderSingle.create().show();

    }


    @Override
    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        Log.i("show", "response; "+jsonObject.toString());
        try{
            int errorCode = jsonObject.getInt("error");
            switch (whichReq){
                case Utility.GET_CATEGORIES_ENTER_TABLES:
                    Log.i("show", "inside GET_CATEGORIES_ENTER_TABLES");
                    if (errorCode == 0) {

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.serializeNulls();
                        Gson gson = gsonBuilder.create();

                        array = jsonObject.getJSONArray("categories");
                        if (array != null) {
                            categoryObject.addAll(Arrays.asList(gson.fromJson(String.valueOf(array), TableCategoryBean[].class)));
                            for (int i = 0; i < categoryObject.size(); i++) {
                                categories.add(categoryObject.get(i).getcName());
                            }
                            adapterCategories.addAll(categories);
                            llCreateTable.setVisibility(View.VISIBLE);
                        }

                    } else if(errorCode == 2){
                        if(enterTablesVisible) {
                            LayoutInflater inflater = LayoutInflater.from(context);
                            View inflatedView = inflater.inflate(R.layout.empty_categories_2, null, false);
                            container.addView(inflatedView);
                            fab.setVisibility(View.GONE);
                        }
                    }
                    Utility.dismissDialog();
                    break;
                case Utility.ADD_TABLE:

                    if(errorCode == 0){
                        Toast.makeText(EnterTablesActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                        clearFields();
                    } else {
                        Toast.makeText(EnterTablesActivity.this, jsonObject.getString("msg"), Toast.LENGTH_SHORT).show();
                    }
                    Utility.dismissDialog();
                    break;

            }

        }catch (JSONException e){
            e.printStackTrace();
        }finally {
            Utility.dismissDialog();
        }

    }


}