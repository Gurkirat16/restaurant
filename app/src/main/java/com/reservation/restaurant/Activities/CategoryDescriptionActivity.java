package com.reservation.restaurant.Activities;

import android.app.Dialog;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.provider.MediaStore;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.android.volley.Request;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.BuildConfig;
import com.reservation.restaurant.Helper.Helper;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.util.HashMap;

import butterknife.BindView;

public class CategoryDescriptionActivity extends AppCompatActivity implements MyResponse, View.OnClickListener{

    //@BindView(R.id.cImg)
    ImageView ivCatImg;
    //@BindView(R.id.cName)
    EditText etxtcName;
    //@BindView(R.id.cDesc)
    EditText etxtcDesc;
    //@BindView(R.id.save_category)
    Button btnSaveCategory;

    TableCategoryBean categoryBean, updatedCategory, newCategory;
    int whichReq;
    int resId, cId;
    MyDB myDB;
    UserSessionManager sessionManager;
    HashMap<String, String> userDetails;

    String base64Image, cName, cDesc;
    File fileName;
    ByteArrayOutputStream bytes;
    URL url = null;
    Bitmap image = null;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_description);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Category Description");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        initMyDB();
        ivCatImg = (ImageView) findViewById(R.id.cImg);
        btnSaveCategory = (Button) findViewById(R.id.save_category);
        etxtcName = (EditText)findViewById(R.id.cName);
        etxtcDesc = (EditText)findViewById(R.id.cDesc);

        updatedCategory = new TableCategoryBean();

        sessionManager = new UserSessionManager(this);
        userDetails = sessionManager.getUserDetails();
        resId = Integer.parseInt(userDetails.get(Utility.SHPRESID));

        i = getIntent();
        whichReq = i.getIntExtra("mode", 0);
        if(whichReq == Utility.ADD_NEW_CATEGORY){
            Picasso.with(getApplicationContext()).load(R.drawable.cocktail).placeholder(R.drawable.cocktail).resize(70,70).into(ivCatImg);
        } else if (whichReq == Utility.UPDATE_CATEGORY){
            categoryBean = (TableCategoryBean) i.getParcelableExtra("categoryBean");
            initCategory();
        }
        ivCatImg.setOnClickListener(CategoryDescriptionActivity.this);
        btnSaveCategory.setOnClickListener(this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Log.i("show", "back");
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(CategoryDescriptionActivity.this);
        myDB.initRequestQueue(CategoryDescriptionActivity.this);
    }

    void initCategory(){
        etxtcName.setText(categoryBean.getcName());
        cId = categoryBean.getcId();
        if(categoryBean.getcDesc() != null)
            etxtcDesc.setText(categoryBean.getcDesc());
        else
            etxtcDesc.setHint("Enter Category description here (for instance, it's seating capacity etc).");
        Utility.dismissDialog();
        initCatImg();
    }

    void initCatImg(){
        String imgUrl = null;
        if(categoryBean.getcImg()!= null)
            imgUrl = categoryBean.getcImg().replaceAll(" ","%20");
        if(imgUrl == null || !imgUrl.contains(".")){
            try{
                Picasso.with(this).load(R.drawable.cocktail).into(ivCatImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else{
//            String url = Uri.parse(categoryBean.getcImg())
//                    .buildUpon()
//                    .build()
//                    .toString();
            try{
//                image = BitmapFactory.decodeStream(url.openConnection().getInputStream());
//                ivCatImg.setImageBitmap(image);
                Picasso.with(this).load(imgUrl).placeholder(R.drawable.cocktail).into(ivCatImg);
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }

    @Override
    public void onClick(View view) {
        if(view.getId()==R.id.save_category){
            //Toast.makeText(this, "btn_save clicked", Toast.LENGTH_SHORT).show();
                cName = etxtcName.getText().toString().trim();
                if(cName.isEmpty()) {
                    etxtcName.setError("Category name is required");
                } else {
                    cDesc = etxtcDesc.getText().toString().trim();
//                if (base64Image.isEmpty()) {
//                    Log.i("show", base64Image);
//                    base64Image = null;
//                }
                    handler.sendEmptyMessage(whichReq);
                }
        }else if(view.getId()==R.id.cImg){
            showCaptureOptions();
        }
    }

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {

                switch (msg.what) {
                    case Utility.ADD_NEW_CATEGORY:
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("resId", resId);
                            jsonObject.put("cName", cName);
                            jsonObject.put("cImg", base64Image);
                            jsonObject.put("cDesc", cDesc);
                            jsonObject.put("singleCategory", 1);
                            myDB.processRequest(Request.Method.POST, Utility.URL_ADD_CATEGORIES, jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case Utility.UPDATE_CATEGORY:
                        try {
                            JSONObject jsonObject = new JSONObject();
                            jsonObject.put("resId", resId);
                            jsonObject.put("cId", cId);
                            jsonObject.put("newCategory", cName);
                            jsonObject.put("newImg", base64Image);
                            jsonObject.put("newDesc", cDesc);
                            myDB.processRequest(Request.Method.POST, Utility.URL_UPDATE_CATEGORY, jsonObject);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }

        }
    };

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try {
            int errorCode = jsonObject.getInt("error");

            switch(whichReq) {

                case Utility.ADD_NEW_CATEGORY:
                    if (errorCode == 0) {
                        cId = jsonObject.getInt("cId");
                        String cImg = jsonObject.getString("cImg");
                        Toast.makeText(getApplicationContext(), "Category Inserted", Toast.LENGTH_LONG).show();
                        newCategory = new TableCategoryBean(cId, cName, resId, cImg, cDesc);
                        i.putExtra("newCategory", (Parcelable)newCategory);
                        setResult(RESULT_OK, i);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Failed to update, please try again later", Toast.LENGTH_LONG).show();
                    }
                    break;

                case Utility.UPDATE_CATEGORY:
                    if (errorCode == 0) {
                        Toast.makeText(getApplicationContext(), "Category Updated", Toast.LENGTH_LONG).show();
                        String cImg = Utility.URL_SERVER_IMGS_FOLDER + cName;
                        updatedCategory = new TableCategoryBean(cId, cName, resId, cImg, cDesc);
                        i.putExtra("updatedCategory", (Parcelable)updatedCategory);
                        setResult(RESULT_OK, i);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Failed to update, please try again later", Toast.LENGTH_LONG).show();
                    }
                    break;


            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    void showCaptureOptions(){
        AlertDialog.Builder builder=new AlertDialog.Builder(this);
        CharSequence options[]={"From Gallery"};
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int position) {
                switch (position){
                  /*  case 0:openCamera();
                        break;*/
                    case 0:pickFromGallery();
                        break;
                }
            }
        });
        builder.create().show();
    }

    void openCamera(){

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        String _imageCapturedName = "temp";
                        /*+ String.valueOf(System.currentTimeMillis());*/
        fileName = Helper.createFileInSDCard(Helper.getTempFile(), _imageCapturedName + ".JPG");
              /*"temp/",*/
        Uri uri = FileProvider.getUriForFile(getApplicationContext(), BuildConfig.APPLICATION_ID+".provider", fileName);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            intent = new Intent("android.media.action.IMAGE_CAPTURE").putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    uri);
        }else{
            intent = new Intent("android.media.action.IMAGE_CAPTURE").putExtra(
                    MediaStore.EXTRA_OUTPUT,
                    Uri.fromFile(new File(fileName.toString())));
        }
        startActivityForResult(intent, Utility.REQ_CODE);

    }

    void pickFromGallery(){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), 101);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 101 && resultCode == RESULT_OK && data != null && data.getData() != null) {
            Uri uri = data.getData();
            try {
                Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), uri);
                ivCatImg.setImageBitmap(bitmap);
                bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 50, bytes);
                byte b[]=bytes.toByteArray();
                base64Image = Base64.encodeToString(b, Base64.DEFAULT);
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else if(requestCode==Utility.REQ_CODE && resultCode==RESULT_OK){
            saveImage(Utility.REQ_CODE);
        }
    }

    private void saveImage(int requestCode) {
        int rotate = 0;
        try {
            File file=new File(Helper.getTempFile()+"temp.JPG");
            BitmapFactory.Options bmOptions = new BitmapFactory.Options();
            Bitmap bitmap = BitmapFactory.decodeFile(file.getAbsolutePath(),bmOptions);
            ExifInterface exif = new ExifInterface(fileName.getAbsolutePath());

            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }

            Matrix matrix = new Matrix();
            matrix.postRotate(rotate);

            bitmap = Bitmap.createScaledBitmap(bitmap,480,320, true);

            Bitmap rotateBitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(),
                    bitmap.getHeight(), matrix, true);

            bytes = new ByteArrayOutputStream();

            rotateBitmap.compress(Bitmap.CompressFormat.JPEG,50,bytes);
            byte b[]=bytes.toByteArray();
            base64Image = Base64.encodeToString(b, Base64.DEFAULT);
            ivCatImg.setImageBitmap(Bitmap.createScaledBitmap(rotateBitmap,140, 140,false));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {

        setResult(RESULT_CANCELED,i);
        finish();
    }

}
