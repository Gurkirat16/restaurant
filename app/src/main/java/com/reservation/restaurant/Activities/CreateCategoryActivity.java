package com.reservation.restaurant.Activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.allattentionhere.fabulousfilter.AAH_FabulousFragment;
import com.android.volley.Request;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.util.Attributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.reservation.restaurant.Adapters.AdminCategoriesAdapter;
import com.reservation.restaurant.Beans.TableCategoryBean;
import com.reservation.restaurant.Fragments.FragmentAddCategory;
import com.reservation.restaurant.Helper.MyDB;
import com.reservation.restaurant.Helper.MyResponse;
import com.reservation.restaurant.Helper.Utility;
import com.reservation.restaurant.OtherClasses.RecyclerItemClickListener;
import com.reservation.restaurant.OtherClasses.UserSessionManager;
import com.reservation.restaurant.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CreateCategoryActivity extends AppCompatActivity implements MyResponse, AAH_FabulousFragment.Callbacks{

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    ArrayList<TableCategoryBean> adminCategories;
    AdminCategoriesAdapter adapter;

    ArrayList<String> globalCategories = new ArrayList<>();
    ArrayList<String> selectedGlobalCategories = new ArrayList<>();
    ArrayList<String> numTablesList = new ArrayList<>();

    @BindView(R.id.rvAdminCategories)
    RecyclerView rvAdminCategories;
    @BindView(R.id.llMainContent)
    LinearLayout llMainContent;
    @BindView(R.id.fabAddCategory)
    FloatingActionButton fab;
    @BindView(R.id.container)
    FrameLayout container;
    View emptyStateView;

    Context context = CreateCategoryActivity.this;

    int resId;
    MyDB myDB;
    int whichRequest;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    TableCategoryBean tableCategoryBean;
    String chosenCategory;
    int position;
    Dialog pd;
    UserSessionManager sessionManager;
    HashMap<String, String> userDetails;
    private static boolean createCategoryVisible = false;
    SwipeLayout swipeLayout;
    //Map<String, String> numTables = new HashMap<>();

    FragmentAddCategory dialogFrag;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_category);
        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Categories");
        setSupportActionBar(mToolbar);

        ButterKnife.bind(this);

        dialogFrag = FragmentAddCategory.newInstance();
        dialogFrag.setParentFab(fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
            }
        });

        pd = Utility.initDialog(context);
        pd.setOnKeyListener(keyListener);

        sharedPreferences = getSharedPreferences(Utility.SHARED_PREF_NAME, 0);
        editor = sharedPreferences.edit();
        editor.putBoolean(Utility.SHP_SHOW_SELECT_CAT, false);
        editor.commit();

        initMyDB();

        sessionManager = new UserSessionManager(this);
        userDetails = sessionManager.getUserDetails();
        resId = Integer.parseInt(userDetails.get(Utility.SHPRESID));

        adminCategories = new ArrayList<>();

        adapter = new AdminCategoriesAdapter(context, adminCategories, numTablesList);
        rvAdminCategories.setLayoutManager(new LinearLayoutManager(this));
        rvAdminCategories.setItemAnimator(new DefaultItemAnimator());
        rvAdminCategories.setAdapter(adapter);
        adapter.setMode(Attributes.Mode.Single);

        whichRequest = Utility.GET_CATEGORIES_LIST;
        handler.sendEmptyMessage(Utility.GET_CATEGORIES_LIST);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initMyDB(){
        myDB = MyDB.getDBObject();
        myDB.initResponse(CreateCategoryActivity.this);
        myDB.initRequestQueue(CreateCategoryActivity.this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        initMyDB();
    }

    @Override
    protected void onStart() {
        super.onStart();
        createCategoryVisible = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        createCategoryVisible = false;
    }

    DialogInterface.OnKeyListener keyListener = new DialogInterface.OnKeyListener() {
        @Override
        public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
            if(i == KeyEvent.KEYCODE_BACK){
                if(whichRequest == Utility.GET_CATEGORIES_LIST) {
                    Utility.dismissDialog();
                    finish();
                }
            }
            return false;
        }
    };

    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case Utility.GET_CATEGORIES_LIST:
                    try {
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        if (createCategoryVisible)
                            myDB.processRequest(Request.Method.POST, Utility.URL_GET_CATEGORIES, jsonObject);
                    } catch (JSONException e) {
                        if (createCategoryVisible)
                            e.printStackTrace();
                    }
                    break;

                case Utility.GET_GLOBAL_CATEGORIES:
                    try {
                        Log.i("show", "GET_GLOBAL_CATEGORIES");
                        JSONObject jsonObject = new JSONObject();
                        jsonObject.put("resId", resId);
                        jsonObject.put("firstTime", false);
                        myDB.processRequest(Request.Method.POST, Utility.URL_GET_GLOBAL_CATEGORIES, jsonObject);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;

                case Utility.ADD_NEW_CATEGORY_GLOBAL_LIST:
                    JSONObject jsonObject = new JSONObject();
                    JSONArray jsonArray = new JSONArray(selectedGlobalCategories);
                    Log.i("show", "jsonArray: " + jsonArray.toString());
                    try {
                        if (jsonArray.length() > 0) {
                            jsonObject.put("categories", jsonArray);
                            jsonObject.put("resId", resId);
                            jsonObject.put("singleCategory", 0);
                            myDB.processRequest(Request.Method.POST, Utility.URL_ADD_CATEGORIES, jsonObject);

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        }
    };

    @Override
    public void onMyResponse(JSONObject jsonObject) {
        try{
            int errorCode = jsonObject.getInt("error");

            switch (whichRequest){
                case Utility.GET_CATEGORIES_LIST:
                    if(errorCode == 0){
                        if(jsonObject.getInt("num_rows") > 0){
                            //llMainContent.setVisibility(View.VISIBLE);
                            JSONArray jsonArray = jsonObject.getJSONArray("categories");
                            JSONArray numTablesArray = jsonObject.getJSONArray("numTables");
                            //numTables = parseJsonObject(numTablesObj);
                            //numTablesList.addAll(Arrays.asList(String.valueOf(numTablesArray)));
                            for (int i = 0; i < numTablesArray.length(); i++) {
                                numTablesList.add(numTablesArray.getString(i));
                                adapter.notifyDataSetChanged();
                            }
                            Log.i("show", "numTablesObject: " + numTablesList.toString());
                            //Log.i("show", "numTables: " + numTables.toString());
                            Log.i("show", "categories: " + jsonArray.toString());
                            GsonBuilder gsonBuilder = new GsonBuilder();
                            gsonBuilder.serializeNulls();
                            Gson gson = gsonBuilder.create();
                            adminCategories.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonArray), TableCategoryBean[].class)));
                            adapter.notifyDataSetChanged();
                        }
                    }else if(errorCode == 2){
                        if(createCategoryVisible){
                            LayoutInflater inflater = LayoutInflater.from(context);
                            emptyStateView = inflater.inflate(R.layout.empty_categories, null, false);
                            container.addView(emptyStateView);
                        }
                    }
                    fetchGlobalCategories();
                    Utility.dismissDialog();
                    break;

                case Utility.GET_GLOBAL_CATEGORIES:
                    if(errorCode == 0) {

                        JSONArray categoryDiff = jsonObject.getJSONArray("category_diff");
                        if(categoryDiff.length() > 0) {
                            for (int i = 0; i < categoryDiff.length(); i++)
                                globalCategories.add((String) categoryDiff.get(i));
                        }

                    }
                    //Utility.dismissDialog();
                    break;

                case Utility.DELETE_CATEGORY:
                    if(errorCode == 0){
                        adapter.mItemManger.removeShownLayouts(swipeLayout);
                        adapter.mItemManger.closeAllItems();
                        adminCategories.remove(position);
                        adapter.notifyDataSetChanged();
                        if(adminCategories.size() == 0){
                            llMainContent.setVisibility(View.GONE);
                        }
                        Toast.makeText(getApplicationContext(), "Category "+chosenCategory+" deleted", Toast.LENGTH_LONG).show();
                        if (!globalCategories.isEmpty()){
                            globalCategories.clear();
                            fetchGlobalCategories();
                        }
                    }else{
                        Toast.makeText(getApplicationContext(), "Please Try again", Toast.LENGTH_LONG).show();
                    }
                    Utility.dismissDialog();
                    break;

                case Utility.ADD_NEW_CATEGORY_GLOBAL_LIST:
                    if(errorCode == 0){

                        GsonBuilder gsonBuilder = new GsonBuilder();
                        gsonBuilder.serializeNulls();
                        Gson gson = gsonBuilder.create();

                        if(container.findViewById(R.id.llNoCategories) != null){
                            container.removeView(emptyStateView);
                        }

                        adminCategories.addAll(Arrays.asList(gson.fromJson(String.valueOf(jsonObject.getJSONArray("categoriesInserted")), TableCategoryBean[].class)));
                        adapter.notifyDataSetChanged();
                        Log.i("show", "categories added");

                        globalCategories.removeAll(selectedGlobalCategories);
                        Log.i("show", "globalCategories after response: " + globalCategories.toString());
                        selectedGlobalCategories.clear();
                        Log.i("show", "selectedGlobalCategories after response: " + selectedGlobalCategories.toString());
                    }else{
                        Toast.makeText(CreateCategoryActivity.this, "Please try again. "+jsonObject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                    Utility.dismissDialog();
                    break;
            }
        }catch (JSONException e){
            e.printStackTrace();
        }

    }

    @Override
    public void onBackPressed() {
        Log.i("show", "finish");
        finish();
    }

    //result from bottom sheet
    @Override
    public void onResult(Object result) {

        if(result instanceof ArrayList){

            if (!(((ArrayList) result).isEmpty())) {

                selectedGlobalCategories.addAll((ArrayList)result);
                pd = Utility.initDialog(context);
                pd.setOnKeyListener(keyListener);
                whichRequest = Utility.ADD_NEW_CATEGORY_GLOBAL_LIST;
                handler.sendEmptyMessage(whichRequest);
            }

        } else if(result.toString().equalsIgnoreCase("create category")){
            Intent i = new Intent(CreateCategoryActivity.this, CategoryDescriptionActivity.class);
            i.putExtra("mode",Utility.ADD_NEW_CATEGORY);
            startActivityForResult(i, Utility.REQ_CODE_NEW_CATEGORY);
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (dialogFrag.isAdded()) {
            dialogFrag.dismiss();
            dialogFrag.show(getSupportFragmentManager(), dialogFrag.getTag());
        }

    }

    public ArrayList<String> getGlobalCategories() {
        return globalCategories;
    }

    void fetchGlobalCategories(){
        whichRequest = Utility.GET_GLOBAL_CATEGORIES;
        handler.sendEmptyMessage(whichRequest);
    }

    //result from CategoryDescriptionActivity
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(resultCode == RESULT_OK && requestCode == Utility.REQ_CODE_UPDATED_CATEGORY){
            TableCategoryBean updatedCategory = data.getParcelableExtra("updatedCategory");
            adminCategories.set(position, updatedCategory);
            adapter.notifyDataSetChanged();
        } else if(resultCode == RESULT_OK && requestCode == Utility.REQ_CODE_NEW_CATEGORY){
            if(container.findViewById(R.id.llNoCategories) != null){
                container.removeView(emptyStateView);
            }
            TableCategoryBean newCategory = data.getParcelableExtra("newCategory");
            adminCategories.add(newCategory);
            adapter.notifyDataSetChanged();
        }
    }


    public void editCategory(int position){
        CreateCategoryActivity.this.position = position;
        CreateCategoryActivity.this.tableCategoryBean = adminCategories.get(position);
        Intent i = new Intent(CreateCategoryActivity.this, CategoryDescriptionActivity.class);
        i.putExtra("mode", Utility.UPDATE_CATEGORY);
        i.putExtra("categoryBean", (Parcelable) adminCategories.get(position));
        startActivityForResult(i, Utility.REQ_CODE_UPDATED_CATEGORY);
    }

    public void deleteCategory(int position, SwipeLayout swipeLayout){
        CreateCategoryActivity.this.position = position;
        tableCategoryBean = adminCategories.get(position);
        CreateCategoryActivity.this.swipeLayout = swipeLayout;
        chosenCategory = tableCategoryBean.getcName();
        whichRequest = Utility.DELETE_CATEGORY;

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Delete");
        builder.setMessage("All tables of this category will be deleted. Are you sure ?");
        builder.setPositiveButton("Delete", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                pd = Utility.initDialog(context);
                try{
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("resId", tableCategoryBean.getResId());
                    jsonObject.put("cId", tableCategoryBean.getcId());
                    myDB.processRequest(Request.Method.POST,Utility.URL_DELETE_CATEGORY,jsonObject);
                }catch (JSONException e){
                    e.printStackTrace();
                }
            }
        });
        builder.setNegativeButton("Cancel",null);
        builder.setCancelable(false);
        builder.create().show();
    }

    public void viewTables(int position){
        CreateCategoryActivity.this.position = position;
        Intent i = new Intent(CreateCategoryActivity.this, ListTablesActivity.class);
        i.putExtra("cId", adminCategories.get(position).getcId());
        startActivity(i);
    }

}