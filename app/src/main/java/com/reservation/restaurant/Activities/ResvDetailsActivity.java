package com.reservation.restaurant.Activities;

import android.animation.AnimatorInflater;
import android.animation.ObjectAnimator;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.animation.Transformation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.reservation.restaurant.Adapters.TablesAdapter;
import com.reservation.restaurant.Beans.Reservations;
import com.reservation.restaurant.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import static android.view.animation.Animation.RELATIVE_TO_SELF;

public class ResvDetailsActivity extends AppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    @BindView(R.id.txtDate)
    TextView txtDate;
    @BindView(R.id.txtTime)
    TextView txtTime;
    @BindView(R.id.txtMembers)
    TextView txtMembers;
    @BindView(R.id.txtCategory)
    TextView txtCategory;
    @BindView(R.id.txtPersonName)
    TextView txtPersonName;
    @BindView(R.id.txtEmail)
    TextView txtEmail;
    @BindView(R.id.txtContact)
    TextView txtContact;
    @BindView(R.id.recViewTables)
    RecyclerView recViewTables;
    @BindView(R.id.llSingleTable)
    LinearLayout llSingleTable;
    @BindView(R.id.llMultipleTables)
    LinearLayout llMultipleTables;
    @BindView(R.id.llViewTables)
    LinearLayout llViewTables;
    @BindView(R.id.txtSingleTable)
    TextView txtSingleTable;
    @BindView(R.id.imgExpand)
    ImageView imgExpand;

    Reservations resv;
    TablesAdapter tablesAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_resv_details);

        Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView mToolBarTextView = (TextView) findViewById(R.id.toolbar_text);
        mToolBarTextView.setText("Details");
        setSupportActionBar(mToolbar);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        ButterKnife.bind(this);

        Intent get = getIntent();
        resv = get.getParcelableExtra("resvDetails");

        Log.i("show", "obj: "+resv.toString());
        initViews();

        tablesAdapter = new TablesAdapter(resv.getArrTableInfo(), 1);
        recViewTables.setLayoutManager(new LinearLayoutManager(this));
        recViewTables.setItemAnimator(new DefaultItemAnimator());
        recViewTables.setAdapter(tablesAdapter);

        imgExpand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                RotateAnimation rotate = null;
                if(llViewTables.getVisibility() == View.VISIBLE){
                    /*rotate = new RotateAnimation(0, 180, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
                    rotate.setDuration(300);
                    //rotate.setFillAfter(true);
                    //imgExpand.setAnimation(rotate);
                    imgExpand.startAnimation(rotate);

                    imgExpand.setRotation(180);
*/
                    ObjectAnimator anim = ObjectAnimator.ofFloat(imgExpand, "rotationX", 180f, 0f);
                   // anim.setTarget(imgExpand);
                    anim.setDuration(300);
                    anim.start();
                    //imgExpand.setRotation(180);
                    collapse(llViewTables);
                }else{
                    /*rotate = new RotateAnimation(180, 0, RELATIVE_TO_SELF, 0.5f, RELATIVE_TO_SELF, 0.5f);
                    rotate.setDuration(300);
                   // rotate.setFillAfter(true);
                    //imgExpand.setAnimation(rotate);
                    imgExpand.startAnimation(rotate);*/
                    //imgExpand.setRotation(180);
                    ObjectAnimator anim = ObjectAnimator.ofFloat(imgExpand, "rotationX", 0f, 180f);
                    //anim.setTarget(imgExpand);
                    anim.setDuration(300);
                    anim.start();

                    expand(llViewTables);
                }
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                Log.i("show", "back");
                finish();
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    void initViews(){
        String strStartTime = resv.getrStartTime();
        String strEndTime = resv.getrEndTime();
        String strDate = resv.getrDate();
        SimpleDateFormat stfo = new SimpleDateFormat("hh:mm:ss");
        SimpleDateFormat stfn = new SimpleDateFormat("hh:mm aa");
        SimpleDateFormat sdfo = new SimpleDateFormat("yyyy-MM-dd");
        SimpleDateFormat sdfn = new SimpleDateFormat("d MMM, yyyy");
        Date date;
        Date startTime, endTime;
        try{
            startTime = stfo.parse(strStartTime);
            endTime = stfo.parse(strEndTime);
            strStartTime = stfn.format(startTime);
            strEndTime = stfn.format(endTime);
            date = sdfo.parse(strDate);
            strDate = sdfn.format(date);
        }catch(ParseException e){
            e.printStackTrace();
        }

        txtDate.setText(strDate);
        txtTime.setText(strStartTime+" - "+strEndTime);
        txtCategory.setText(resv.getrCategoryInfo().getcName());
        txtMembers.setText(String.valueOf(resv.getrCusInfo().getNumMembers()));
        txtContact.setText(resv.getrCusInfo().getCusPhone());
        txtEmail.setText(resv.getrCusInfo().getCusEmail());
        txtPersonName.setText(resv.getrCusInfo().getCusName());
        if(resv.getArrTableInfo().size() == 1){
            llMultipleTables.setVisibility(View.GONE);
            llSingleTable.setVisibility(View.VISIBLE);
            txtSingleTable.setText(resv.getArrTableInfo().get(0).getTableName());
        }else{
            llMultipleTables.setVisibility(View.VISIBLE);
            llSingleTable.setVisibility(View.GONE);
            llViewTables.setVisibility(View.GONE);
        }

    }
    public static void expand(final View v) {
        v.measure(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? LinearLayout.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }
}

