package com.reservation.restaurant.Helper;

import org.json.JSONObject;

/**
 * Created by mitaly on 19/9/17.
 */

public interface MyResponse {
    void onMyResponse(JSONObject jsonObject);
}
