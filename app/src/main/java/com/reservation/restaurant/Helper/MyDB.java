package com.reservation.restaurant.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONObject;

/**
 * Created by mitaly on 19/9/17.
 */

public class MyDB {

    Context context;
    JsonObjectRequest jsonObjectRequest;
    MyResponse myResponse;
    RequestQueue requestQueue;
    static MyDB db = new MyDB();

    private MyDB() {

    }

    public static MyDB getDBObject(){
        return db;
    }

    public void initRequestQueue(Context context){
        this.context = context;
        requestQueue = Volley.newRequestQueue(context);
    }
    public void initResponse(MyResponse myResponse){
        this.myResponse = myResponse;
    }

    public void registerMyListener(MyResponse myResponse){
        this.myResponse = myResponse;
    }

    public void processRequest(int method, String url, final JSONObject jsonObject) {
        if (Utility.isNetworkConnected(((Activity) context))) {
            jsonObjectRequest = new JsonObjectRequest(method, url, jsonObject, new Response.Listener<JSONObject>() {

                @Override
                public void onResponse(JSONObject response) {
                    Log.i("show", "response: " + response.toString());
                    myResponse.onMyResponse(response);
                }
            },
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            Log.i("show", "Error: " + error.toString());
                        }
                    });

            jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    20000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));

            requestQueue.add(jsonObjectRequest);

        } else{
            Toast.makeText(context, "Network Connection Error!", Toast.LENGTH_SHORT).show();
            Utility.dismissDialog();
            ((Activity)context).finish();
        }

    }
}
