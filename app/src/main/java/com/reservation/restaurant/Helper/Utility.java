package com.reservation.restaurant.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.view.Window;

import com.airbnb.lottie.LottieAnimationView;
import com.reservation.restaurant.Activities.ListTablesActivity;
import com.reservation.restaurant.R;

/**
 * Created by mitaly on 19/9/17.
 */


public class Utility {

    public static final String URL = "http://www.auribises.net/auriresturant/";

    public static final String URL_ADD_CATEGORIES = URL+"add_category.php";
    public static final String URL_GET_CATEGORIES = URL+"get_categories.php";
    public static final String URL_GET_CATEGORY_COUNT = URL+"get_category_count.php";
    public static final String URL_DELETE_CATEGORY = URL+"delete_category.php";
    public static final String URL_LOGIN_GET_ADMIN = URL+"login.php";
    public static final String URL_GET_GLOBAL_CATEGORIES = URL+"get_global_categories.php";
    public static final String URL_GET_RESERVATIONS = URL+"get_reservations.php";
    public static final String URL_GET_TABLES = URL+"get_tables.php";
    public static final String URL_DELETE_TABLE = URL+"delete_table.php";
    public static final String URL_UPDATE_TABLE = URL+"update_table.php";
    public static final String URL_GET_TABLES_WITH_LOCATION = URL+"get_tables_with_location.php";
    public static final String URL_ADD_TABLE = URL+"add_table.php";
    public static final String URL_SET_LOCATION = URL+"set_location.php";
    public static final String URL_MERGE_TABLES = URL+"merge_tables.php";
    public static final String URL_GET_MERGE_COMBN = URL+"get_merge_combinations.php";
    public static final String URL_DELETE_MERGE_CMB = URL+"delete_merge_cmb.php";
    public static final String URL_DELETE_MERGED_TABLE = URL+"delete_merged_table.php";
    public static final String URL_UPDATE_CATEGORY = URL+"update_category.php";
    public static final String URL_SERVER_IMGS_FOLDER = URL+"imgs/";
    public static final String URL_GET_RESV_REQUESTS = URL+"get_resv_requests.php";
    public static final String URL_ACTION_RESV_REQ = URL+"action_resv_req.php";
    public static final String URL_GET_ACTIVE_RESV = URL+"get_active_resv.php";
    public static final String URL_ACTION_RESV_OVER = URL+"action_resv_over.php";
    public static final String URL_GET_AVAILABLE_TABLES = URL+"get_available_tables.php";

    public static Dialog dialog;

    //CreateCategoryActivity
    public static final int GET_CATEGORIES_LIST = 100;
    public static final int ADD_NEW_CATEGORY = 101;
    public static final int DELETE_CATEGORY = 102;
    public static final int RENAME_CATEGORY = 103;
    public static final int GET_GLOBAL_CATEGORIES = 104;
    public static final int UPDATE_CATEGORY = 105;
    public static final int REQ_CODE_UPDATED_CATEGORY = 106;
    public static final int REQ_CODE_NEW_CATEGORY = 107;

    //ListTablesActivity
    public static final int GET_TABLES = 200;
    public static final int DELETE_TABLE = 202;
    public static final int UPDATE_TABLE = 203;

    //SelectCategoryActivity
    public static final int RESULT_CODE_GLOBAL_CAT = 101;
    public static final int GET_GLOBAL_CATEGORIES_FIRST_TIME = 300;
    public static final int ADD_NEW_CATEGORY_GLOBAL_LIST = 301;

    // Dashboard Activity
    public static final int GET_CATEGORY_COUNT = 400;

    // Notifications Activity
    public static final int GET_RESERVATIONS = 500;

    //FloormapActivity
    public static final int MOVE_TO_FRAGMENT = 600;

    //FloormapFragment
    public static final int SET_LOCATION_TABLE = 701;
    public static final int MERGE_TABLES = 702;
    public static final int VIEW_MERGE_COMBN = 703;

    //EnterTablesActivity
    public static final int GET_CATEGORIES_ENTER_TABLES = 800;
    public static final int ADD_TABLE = 801;

    //MergeCombinationsActivity
    public static final int DELETE_MERGE_CMB = 900;
    public static final int  DELETE_MERGED_TABLE = 901;

    //CategoryDescriptionActivity
    public static final int REQ_CODE = 902;

    //ResvRequestActivity
    public static final int GET_RESV_REQUEST = 1000;
    public static final int ACTION_RESV_REQ = 1001;
    public static final int GET_ACTIVE_RESV = 1002;
    public static final int ACTION_RESV_OVER = 1003;

    public static final int REQ_CODE_CHECK_INTERNET = 2001;

    //WalkInsActivity
    public static final int GET_AVAILABLE_TABLES = 3001;

    public static final String SHARED_PREF_NAME = "loginSharedPref";
    public static final String IS_USER_LOGIN = "isUserLoggedIn";
    public static final String SHPEMAIL = "email";
    public static final String SHPPASSWORD = "password";
    public static final String SHPNAME = "name";
    public static final String SHPID = "id";
    public static final String SHPRESID = "resID";
    public static final String SHP_SHOW_SELECT_CAT = "showSelectCategory";
    public static final String SHPTOKEN = "token";

    public static boolean isNetworkConnected(Activity activity) {
        ConnectivityManager cmgr;
        cmgr= (ConnectivityManager)activity.getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo info=cmgr.getActiveNetworkInfo();
        if(info!=null){
            if(info.isConnected()){
                return true;
            }else{
                return false;
            }
        }else {
            return false;
        }
    }

    public static Dialog initDialog(Context context){
        dialog = new Dialog(context);
        dialog .requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.progress_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(false);
        dialog.show();
        LottieAnimationView animationView = (LottieAnimationView)dialog.findViewById(R.id.animation_view);
        animationView.setAnimation("preloader.json");
        animationView.loop(true);
        animationView.setProgress(0.5f);
        animationView.playAnimation();
        return dialog;
    }

    public static void dismissDialog(){
        dialog.dismiss();
    }
}
